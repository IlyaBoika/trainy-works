﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Validators
{
    public class CustomRuleValidator : ICustomRuleValidator
    {
        private readonly IRuleManager _ruleManager;

        public CustomRuleValidator(IRuleManager ruleManager)
        {
            _ruleManager = ruleManager;
        }

        public IEnumerable<RuleCheckResult> Validate(string inputApi)
        {
            var inputApiInJObject = JObject.Parse(inputApi);
            var resultErrorList = new List<RuleCheckResult>();
            var rulesToCheck = _ruleManager.GetCustomRules();

            resultErrorList.AddRange(ReceiveNotices(rulesToCheck, inputApiInJObject));

            if (!(resultErrorList.Any()))
            {
                resultErrorList.Add(new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Success,
                    Message = "Validation on Custom rules"
                });
            }

            return resultErrorList;
        }

        private IEnumerable<RuleCheckResult> ReceiveNotices(IEnumerable<ICustomRule> httpRules, JObject inputApiInJObject)
        {
            var noticesList = new List<RuleCheckResult>();

            foreach (var rule in httpRules)
            {
                var errors = rule.Check(inputApiInJObject).ToList();
                noticesList.AddRange(errors);
            }

            return noticesList;
        }
    }
}