﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Validators
{
    public class HttpMethodValidator : IHttpMethodValidator
    {
        private readonly IRuleManager _ruleManager;

        public HttpMethodValidator(IRuleManager ruleManager)
        {
            _ruleManager = ruleManager;
        }

        public IEnumerable<RuleCheckResult> Validate(string inputApi)
        {
            var inputApiInJObject = JObject.Parse(inputApi);

            var getHttpNotices = ReceiveNotices(_ruleManager.GetHttpRules(HttpMethodType.Get), inputApiInJObject);
            var headHttpNotices = ReceiveNotices(_ruleManager.GetHttpRules(HttpMethodType.Head), inputApiInJObject);
            var postHttpNotices = ReceiveNotices(_ruleManager.GetHttpRules(HttpMethodType.Post), inputApiInJObject);
            var deleteHttpNotices = ReceiveNotices(_ruleManager.GetHttpRules(HttpMethodType.Delete), inputApiInJObject);
            var putHttpNotices = ReceiveNotices(_ruleManager.GetHttpRules(HttpMethodType.Put), inputApiInJObject);

            var resultErrorList = new List<RuleCheckResult>();

            resultErrorList.AddRange(getHttpNotices);
            resultErrorList.AddRange(headHttpNotices);
            resultErrorList.AddRange(postHttpNotices);
            resultErrorList.AddRange(deleteHttpNotices);
            resultErrorList.AddRange(putHttpNotices);

            if (!(resultErrorList.Any()))
            {
                resultErrorList.Add( new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Success,
                    Message = "Validation on Http-rules"
                });
            }

            return resultErrorList;
        }

        private IEnumerable<RuleCheckResult> ReceiveNotices(IEnumerable<IHttpMethodRule> httpRules, JObject inputApiInJObject)
        {
            var noticesList = new List<RuleCheckResult>();

            foreach (var rule in httpRules)
            {
              var errors = rule.Check(inputApiInJObject).ToList();
              noticesList.AddRange(errors);
            }

            return noticesList;
        }
    }
}