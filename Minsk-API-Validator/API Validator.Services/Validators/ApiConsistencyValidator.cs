﻿using System;
using System.Collections.Generic;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace API_Validator.Services.Validators
{
    public class ApiConsistencyValidator : IApiConsistencyValidator
    {
        private List<RuleCheckResult> _result = new List<RuleCheckResult>();
        private JToken _json;

        public IEnumerable<RuleCheckResult> Validate(string inputApi)
        {
            if (IsValidJson(inputApi, ref _result, ref _json))
            {
                // load schema
                JSchema schema = JSchema.Parse(JsonSchema.Schema);

                // validate json
                bool valid = _json.IsValid(schema, out IList<ValidationError> errors);

                if (valid) return _result;

                foreach (var error in errors)
                {
                    _result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = error.LineNumber,
                        Message = error.Message
                    });
                }

                return _result;
            }

            return _result;
        }

        private static bool IsValidJson(string strInput, ref List<RuleCheckResult> result, ref JToken json)
        {
            strInput = strInput.Trim();

            if ((!strInput.StartsWith("{")))
            {
                result.Add(new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Error,
                    Message = "Chosen file is not consistent: missing opening '{' file character"
                });

                return false;
            }

            if ((!strInput.EndsWith("}")))
            {
                result.Add(new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Error,
                    Message = "Chosen file is not consistent: missing closing '}' file character"
                });

                return false;
            }

            try
            {
                 json = JToken.Parse(strInput);
                return true;
            }
            catch (JsonReaderException jex)
            {
                result.Add(new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Error,
                    Message = "Chosen file is not consistent"
                });

                //Exception in parsing json
                result.Add(new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Error,
                    Message = jex.Message
                });

                return false;
            }
            catch (Exception ex) //some other exception
            {
                result.Add(new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Error,
                    Message = ex.Message
                });


                return false;
            }
        }
    }
}