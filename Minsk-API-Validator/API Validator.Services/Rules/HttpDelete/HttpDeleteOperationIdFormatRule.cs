﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpDelete
{
    public class HttpDeleteOperationIdFormatRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();

            IEnumerable<JToken> choosenTokens = input.SelectTokens("$..delete").ToArray();

            foreach (var token in choosenTokens)
            {
                var pathToken = token.Path.Replace("'].delete", string.Empty);

                if (pathToken.EndsWith("}"))
                {
                    var tagsToken = token["tags"];

                    if (tagsToken.First == null)
                    {
                        IJsonLineInfo tokenInfo = token;
                        result.Add(new RuleCheckResult()
                        {
                            ResultType = RuleCheckResultType.Error,
                            Line = tokenInfo.LineNumber,
                            Message = "Add resource name to tag array"
                        });
                    }
                    else
                    {
                        var resName = tagsToken.First.ToString();
                        var operationId = token["operationId"].ToString();

                        if ((!(operationId.StartsWith("Delete"))) && (!(operationId.ToLower().EndsWith(resName))))
                        {
                            IJsonLineInfo tokenInfo = token["operationId"];
                            result.Add(new RuleCheckResult()
                            {
                                ResultType = RuleCheckResultType.Error,
                                Line = tokenInfo.LineNumber,
                                Message = $"OperationId should start with 'Delete' and end with '{resName}'"
                            });
                        }
                    }
                }
            }

            return result;
        }
    }
}