﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpDelete
{
    public class HttpDeleteResponse204HeaderExistRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();

            IEnumerable<JToken> choosenTokens = input.SelectTokens("$..delete.responses.204.headers").ToArray();

            foreach (var token in choosenTokens)
            {
                var desiredToken = token["Content-Type"];

                if (desiredToken != null)
                {
                    IJsonLineInfo tokenInfo = desiredToken;
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = tokenInfo.LineNumber,
                        Message = "Content-Type should not be returned when deleting a resource"
                    });
                }
            }

            return result;
        }
    }
}