﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpDelete
{
    public class HttpDeleteResponse204SchemaExpectedRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();

            IEnumerable<JToken> choosenTokens = input.SelectTokens("$..delete.responses.204").ToArray();

            foreach (var token in choosenTokens)
            {
                var desiredToken = token["schema"];

                if (desiredToken != null)
                {
                    var refInDesiredToken = desiredToken["$ref"];

                    if ((refInDesiredToken != null))
                    {
                        IJsonLineInfo tokenInfo = refInDesiredToken;

                        result.Add(new RuleCheckResult()
                        {
                            ResultType = RuleCheckResultType.Error,
                            Line = tokenInfo.LineNumber,
                            Message = "No Schema expected when deleting a resource"
                        });
                    }
                }
            }

            return result;
        }
    }
}