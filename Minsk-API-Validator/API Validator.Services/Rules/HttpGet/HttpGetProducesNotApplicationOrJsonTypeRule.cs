﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpGet
{
    public class HttpGetProducesNotApplicationOrJsonTypeRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();

            IEnumerable<JToken> choosenTokens = input.SelectTokens("$..get.produces").ToArray();

            foreach (var token in choosenTokens)
            {
                if (!(token.ToString().Contains("application/json")))
                {
                    IJsonLineInfo tokenInfo = token;

                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Warning,
                        Line = tokenInfo.LineNumber,
                        Message = "Application/json is typical mime type when returning a resource"
                    });
                }
            }

            return result;
        }
    }
}