﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpGet
{
    public class HttpGetHeaderOfContentTypeExpectedRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();

            IEnumerable<JToken> choosenTokens = input.SelectTokens("$..get.responses.200.headers").ToArray();

            foreach (var token in choosenTokens)
            {
                var desiredToken = token["Content-Type"];

                if (desiredToken == null)
                {
                    IJsonLineInfo tokenInfo = token;

                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = tokenInfo.LineNumber,
                        Message = "Content-Type header expected when returning a resource"
                    });
                }
            }

            return result;
        }
    }
}