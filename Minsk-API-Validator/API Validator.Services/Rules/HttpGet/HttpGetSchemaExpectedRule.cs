﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpGet
{
    public class HttpGetSchemaExpectedRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();

            IEnumerable<JToken> choosenTokens = input.SelectTokens("$..get.responses.200.schema").ToArray();

            foreach (var token in choosenTokens)
            {
                var desiredToken = token["$ref"];

                if (desiredToken == null)
                {
                    IJsonLineInfo test = token;
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = test.LineNumber,
                        Message = "Schema expected when returning a resource"
                    });
                }
            }

            return result;
        }
    }
}