﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpGet
{
    public class HttpGetOperationIdFormatRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();

            IEnumerable<JToken> choosenTokens = input.SelectTokens("$..get").ToArray();

            foreach (var token in choosenTokens)
            {
                var operationIdToken = token["operationId"].ToString();
                var resName = token["responses"]?["200"]?["schema"]?["$ref"]?.ToString().Split('/').Last();

                if (resName == null)
                {
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Warning,
                        Line = ((IJsonLineInfo)token).LineNumber,
                        Message = $"Rule check fail: \"$ref\" not found for path: {token.Path}/responses/200/schema/$ref"
                    });
                    continue;
                }

                if ((!(operationIdToken.EndsWith(resName))))
                {
                    IJsonLineInfo tokenInfo = token["operationId"];

                    if ((!(operationIdToken.StartsWith("Get"))) && (!(operationIdToken.EndsWith(resName))))
                    {
                        result.Add(new RuleCheckResult()
                        {
                            ResultType = RuleCheckResultType.Error,
                            Line = tokenInfo.LineNumber,
                            Message = $"OperationId should start with 'Get' and end with {resName}"
                        });
                        continue;
                    }

                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = tokenInfo.LineNumber,
                        Message = $"OperationId should end with {resName}"
                    });
                }
            }

            return result;
        }
    }
}