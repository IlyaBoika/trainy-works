﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpGet
{
    public class HttpGetSchemaDefenitionNameRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();

            IEnumerable<JToken> choosenTokens = input.SelectTokens("$..get.responses.200.schema.$ref").ToArray();

            foreach (var token in choosenTokens)
            {
                if (!(token.ToString().EndsWith("Collection")))
                {
                    IJsonLineInfo tokenInfo = token;
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Warning,
                        Line = tokenInfo.LineNumber,
                        Message = "Schema definition name should be a resource collection, suffixed with 'Collection'"
                    });
                }

                if (token.ToString().EndsWith("Value"))
                {
                    IJsonLineInfo tokenInfo = token;
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = tokenInfo.LineNumber,
                        Message = "Schema definition name should be a resource and not end with 'Value'"
                    });
                }

                else if (token.ToString().EndsWith("Ref"))
                {
                    IJsonLineInfo tokenInfo = token;
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = tokenInfo.LineNumber,
                        Message = "Schema definition name should be a resource and not end with 'Ref'"
                    });
                }

                else if (token.ToString().EndsWith("ValueCollection"))
                {
                    IJsonLineInfo tokenInfo = token;
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Warning,
                        Line = tokenInfo.LineNumber,
                        Message = "Schema definition name should be a resource collection, suffixed with 'Collection'"
                    });
                }

                else if (token.ToString().EndsWith("RefCollection"))
                {
                    IJsonLineInfo tokenInfo = token;
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Warning,
                        Line = tokenInfo.LineNumber,
                        Message = "Schema definition name should be a resource collection and not end with 'RefCollection'"
                    });
                }
            }

            return result;
        }
    }
}