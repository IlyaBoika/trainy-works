﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.CustomRules
{
    public class AlphanumericRule : ICustomRule
    {
        public int Id { get; set; }

        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();

            IEnumerable<JToken> choosenTokens = input.SelectTokens("$..$ref").ToArray();

            foreach (var token in choosenTokens)
            {
                var condition = "^[a-zA-Z0-9\\/\\#]+$";

                if (!(Regex.IsMatch(token.ToString(), condition)))
                {
                    IJsonLineInfo tokenInfo = token;

                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = tokenInfo.LineNumber,
                        Message = "Wrong reference declaration: $ref must be alphanumeric"
                    });
                }
            }

            return result;
        }
    }
}
