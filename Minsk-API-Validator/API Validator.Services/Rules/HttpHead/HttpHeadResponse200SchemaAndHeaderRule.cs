﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpHead
{
    public class HttpHeadResponse200SchemaAndHeaderRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();

            IEnumerable<JToken> choosenTokens = input.SelectTokens("$..head.responses.200").ToArray();

            foreach (var token in choosenTokens)
            {
                var schemaToken = token["schema"];

                var headersToken = token["headers"];

                if (schemaToken != null)
                {
                    IJsonLineInfo tokenInfo = schemaToken;

                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = tokenInfo.LineNumber,
                        Message = "Schema not expected when head"
                    });
                }

                if (headersToken != null)
                {
                    var contentTypeToken = token["Content-Type"];

                    if (contentTypeToken != null)
                    {
                        IJsonLineInfo tokenInfo = contentTypeToken;

                        result.Add(new RuleCheckResult()
                        {
                            ResultType = RuleCheckResultType.Error,
                            Line = tokenInfo.LineNumber,
                            Message = "Content-Type header not expected when head"
                        });
                    }
                }
            }

            return result;
        }
    }
}