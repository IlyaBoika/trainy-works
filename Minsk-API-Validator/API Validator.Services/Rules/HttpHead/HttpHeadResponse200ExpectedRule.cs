﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpHead
{
    public class HttpHeadResponse200ExpectedRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();

            IEnumerable<JToken> choosenTokens = input.SelectTokens("$..head.responses").ToArray();

            foreach (var token in choosenTokens)
            {
                var desiredToken = token["200"];

                if (desiredToken == null)
                {
                    IJsonLineInfo tokenInfo = token;

                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = tokenInfo.LineNumber,
                        Message = "Response 200 expected when head"
                    });
                }
            }

            return result;
        }
    }
}