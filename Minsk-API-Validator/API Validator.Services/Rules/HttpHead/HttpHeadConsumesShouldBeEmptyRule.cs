﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpHead
{
    public class HttpHeadConsumesShouldBeEmptyRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();

            IEnumerable<JToken> choosenTokens = input.SelectTokens("$..head.consumes").ToArray();

            foreach (var token in choosenTokens)
            {
                if ((token.First != null))
                {
                    IJsonLineInfo tokenInfo = token.First;

                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = tokenInfo.LineNumber,
                        Message = "Consumes should be empty when head"
                    });
                }
            }

            return result;
        }
    }
}