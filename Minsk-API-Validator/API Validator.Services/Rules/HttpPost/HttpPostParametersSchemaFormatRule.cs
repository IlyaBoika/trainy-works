﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpPost
{
    public class HttpPostParametersSchemaFormatRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();
            var allPostMethodsParameters = input.SelectTokens("$..post.parameters").ToArray();
            foreach (var parameters in allPostMethodsParameters)
            {
                foreach (var parameter in parameters)
                {
                    if (parameter["in"].ToString() == "body" && parameter["schema"] != null)
                    {
                        var schemeVal = parameter["schema"]["$ref"]?.ToString();
                        if (schemeVal == null)
                        {
                            result.Add(new RuleCheckResult()
                            {
                                ResultType = RuleCheckResultType.Warning,
                                Line = ((IJsonLineInfo)parameter["schema"]).LineNumber,
                                Message = $"Rule check fail: \"$ref\" not found for path: {parameter.Path}/schema/$ref"
                            });
                            continue;
                        }

                        if (schemeVal.EndsWith("Ref"))
                        {
                            result.Add(new RuleCheckResult()
                            {
                                ResultType = RuleCheckResultType.Warning,
                                Line = ((IJsonLineInfo) parameter["schema"]).LineNumber,
                                Message = "Schema definition name should be a resource and not end with \"Ref\""
                            });
                        }

                        if (schemeVal.EndsWith("Value"))
                        {
                            result.Add(new RuleCheckResult()
                            {
                                ResultType = RuleCheckResultType.Error,
                                Line = ((IJsonLineInfo) parameter["schema"]).LineNumber,
                                Message = "Schema definition name should be a resource and not end with \"Value\""
                            });
                        }
                    }
                }
            }

            return result;
        }
    }
}