﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpPost
{
    public class HttpPostApplicationOrJsonShouldBeProducedIfAcceptHeaderSpecifiedRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();
            var allPostMethods = input.SelectTokens("$..post").ToArray();
            foreach (var method in allPostMethods)
            {
                var produces = method["produces"]?.ToString();

                if (produces == null)
                {
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = ((IJsonLineInfo) method).LineNumber,
                        Message = "Produces is missing"
                    });

                    return result;
                }

                var hasAcceptHeader = false;
                var parameters = method["parameters"];

                foreach (var parameter in parameters)
                {
                    if (parameter["in"].ToString() == "header" && parameter["name"].ToString() == "Accept")
                    {
                        hasAcceptHeader = true;
                    }
                }

                if(hasAcceptHeader && produces.Contains("application/json"))
                {
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Warning,
                        Line = ((IJsonLineInfo) method).LineNumber,
                        Message = "Application/json should only be produced if an accept header is specified"
                    });
                }
            }

            return result;
        }
    }
}