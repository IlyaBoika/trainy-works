﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpPost
{
    public class HttpPostResponse201Rules : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();
            var allPostMethodsResponses = input.SelectTokens("$..post.responses").ToArray();

            foreach (var responses in allPostMethodsResponses)
            {
                var check201ExistsInfo = CheckResponse201Exists(responses);
                if(check201ExistsInfo.ResultType == RuleCheckResultType.Success)
                {
                    var response201 = responses["201"];
                    var contentTypeHeaderNotExpectedInfo = CheckContentTypeHeaderNotExpected(response201);
                    if(contentTypeHeaderNotExpectedInfo.ResultType != RuleCheckResultType.Success)
                    {
                        result.Add(contentTypeHeaderNotExpectedInfo);
                    }

                    var locationHeaderExpectedinfo = CheckLocationHeaderExpected(response201);
                    if(locationHeaderExpectedinfo.ResultType != RuleCheckResultType.Success)
                    {
                        result.Add(locationHeaderExpectedinfo);
                    }

                    var schemeNotExistsinfo = CheckSchemeNotExists(response201);
                    if (schemeNotExistsinfo.ResultType != RuleCheckResultType.Success)
                    {
                        result.Add(schemeNotExistsinfo);
                    }
                }
                else
                {
                    result.Add(check201ExistsInfo);
                }
            }

            return result;
        }

        private RuleCheckResult CheckResponse201Exists(JToken responses)
        {
            var block = responses["201"];
            if (block == null)
            {
                return new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Error,
                    Line = ((IJsonLineInfo) responses).LineNumber,
                    Message = "Response 201 expected when creating a resource"
                };
            }

            return new RuleCheckResult()
            {
                ResultType = RuleCheckResultType.Success
            };
        }

        private RuleCheckResult CheckContentTypeHeaderNotExpected(JToken response)
        {
            var headers = response["headers"];
            var contentType = headers["Location"];

            if (contentType == null)
            {
                return new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Success
                };
            }

            return new RuleCheckResult()
            {
                ResultType = RuleCheckResultType.Warning,
                Line = ((IJsonLineInfo) headers).LineNumber,
                Message = "Content-Type header not expected in response 201 when creating a resource"
            };
        }

        private RuleCheckResult CheckLocationHeaderExpected(JToken response)
        {
            var headers = response["headers"];
            var location = headers["Location"];
            if (location == null)
            {
                return new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Error,
                    Line = ((IJsonLineInfo) headers).LineNumber,
                    Message = "Location header expected in response201 when creating a resource"
                };
            }

            return new RuleCheckResult()
            {
                ResultType = RuleCheckResultType.Success
            };
        }

        private RuleCheckResult CheckSchemeNotExists(JToken response)
        {
            var scheme = response["schema"];
            if(scheme == null)
            {
                return new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Success
                };
            }

            return new RuleCheckResult()
            {
                ResultType = RuleCheckResultType.Warning,
                Line = ((IJsonLineInfo) response).LineNumber,
                Message = "Schema not expected in response 201 when creating a resource"
            };
        }
    }
}