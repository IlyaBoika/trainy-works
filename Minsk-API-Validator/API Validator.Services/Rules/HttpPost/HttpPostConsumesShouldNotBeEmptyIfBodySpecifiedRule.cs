﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpPost
{
    public class HttpPostConsumesShouldNotBeEmptyIfBodySpecifiedRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();
            var allPostMethods = input.SelectTokens("$..post").ToArray();
            foreach (var method in allPostMethods)
            {
                var consumes = method["consumes"];
                var hasBody = false;
                var parameters = method["parameters"];

                if (parameters == null)
                {
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = ((IJsonLineInfo) method).LineNumber,
                        Message = "Parameters is missing"
                    });

                    if (consumes == null)
                    {
                        result.Add(new RuleCheckResult()
                        {
                            ResultType = RuleCheckResultType.Error,
                            Line = ((IJsonLineInfo) method).LineNumber,
                            Message = "Consumes is missing"
                        });

                        return result;
                    }
                }

                if (parameters != null)
                    foreach (var parameter in parameters)
                    {
                        if (parameter["in"].ToString() == "body")
                        {
                            hasBody = true;
                        }
                    }

                if (hasBody && consumes.ToArray().Count() == 0)
                {
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = ((IJsonLineInfo) consumes).LineNumber,
                        Message = "Consumes should not be empty if a body parameter is specified"
                    });
                }
            }

            return result;
        }
    }
}