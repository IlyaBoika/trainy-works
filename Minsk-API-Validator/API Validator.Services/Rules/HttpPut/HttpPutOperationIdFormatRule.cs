﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpPut
{
    public class HttpPutOperationIdFormatRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();
            var allPostMethods = input.SelectTokens("$..put").ToArray();
            foreach (var method in allPostMethods)
            {
                var operationId = method["operationId"];
                var resName = string.Empty;
                var parameters = method["parameters"];

                if (parameters == null)
                {
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Warning,
                        Line = ((IJsonLineInfo)method).LineNumber,
                        Message = $"Rule check fail: \"$ref\" not found for path: {method.Path}/schema/$ref"
                    });
                    continue;
                }

                foreach (var parameter in parameters)
                {
                    if (parameter["in"].ToString() == "body" && parameter["schema"] != null)
                    {
                        resName = parameter["schema"]["$ref"].ToString().Split('/').Last();
                    }
                }

                if (!operationId.ToString().StartsWith("Update"))
                {
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Warning,
                        Line = ((IJsonLineInfo) operationId).LineNumber,
                        Message = " OperationId should start with \"Update\""
                    });
                }

                if (!string.IsNullOrEmpty(resName) && !operationId.ToString().EndsWith(resName))
                {
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Warning,
                        Line = ((IJsonLineInfo) operationId).LineNumber,
                        Message = $"OperationId should end with \"{resName}\""
                    });
                }
            }

            return result;
        }
    }
}