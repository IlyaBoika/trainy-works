﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpPut
{
    public class HttpPutParametersSchemaDefenitionNameShouldBeResourceCollectionRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();
            var getUrlRegex = new Regex("\\['(.+)'\\]");
            var allPutMethods = input.SelectTokens(("$..put"));
            var urlRegex = new Regex(".*[a-z]$");
           foreach (var endpoint in allPutMethods)
           {
               if (endpoint == null)
               {
                   continue;
               }

                var url = getUrlRegex.Match(endpoint.Path).Groups[1].Value;
               if (urlRegex.IsMatch(url))
                {
                    if (endpoint["parameters"] == null)
                    {
                        result.Add(new RuleCheckResult()
                        {
                            ResultType = RuleCheckResultType.Warning,
                            Line = ((IJsonLineInfo)endpoint).LineNumber,
                            Message = $"Rule check fail: \"parameters\" not found for path: {endpoint.Path}/parameters"
                        });
                        continue;
                    }

                    foreach (var parameter in endpoint["parameters"])
                    {
                        if (parameter["in"].ToString() == "body" && parameter["schema"] != null)
                        {
                           var refVal = parameter["schema"]["$ref"];
                           if(!refVal.ToString().EndsWith("Collection"))
                           {
                               result.Add(new RuleCheckResult()
                               {
                                   ResultType = RuleCheckResultType.Error,
                                   Line = ((IJsonLineInfo) refVal).LineNumber,
                                   Message = "Schema definition name should be a resource collection, suffixed with \"Collection\""
                               });
                           }
                           else if(refVal.ToString().EndsWith("ValueCollection"))
                           {
                               result.Add(new RuleCheckResult()
                               {
                                   ResultType = RuleCheckResultType.Error,
                                   Line = ((IJsonLineInfo) refVal).LineNumber,
                                   Message = "Schema definition name should be a resource collection and not end with \"ValueCollection\""
                               });
                           }
                           else if (refVal.ToString().EndsWith("RefCollection"))
                           {
                               result.Add(new RuleCheckResult()
                               {
                                   ResultType = RuleCheckResultType.Error,
                                   Line = ((IJsonLineInfo) refVal).LineNumber,
                                   Message = "Schema definition name should be a resource collection and not end with \"RefCollection\""
                               });
                           }
                        }
                    }
                }
           }

            return result;
        }
    }
}