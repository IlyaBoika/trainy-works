﻿using System.Collections.Generic;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpPut
{
    public class HttpPutParametersSchemaDefenitionNameRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();
            var allMethodsParameters = input.SelectTokens(("$..put.parameters"));
            foreach (var parameters in allMethodsParameters)
            {
                foreach (var parameter in parameters)
                {
                    if (parameter["in"].ToString() == "body" && parameter["schema"] != null)
                    {
                        var refVal = parameter["schema"]["$ref"];
                        if (refVal.ToString().EndsWith("Value"))
                        {
                            result.Add(new RuleCheckResult()
                            {
                                ResultType = RuleCheckResultType.Error,
                                Line = ((IJsonLineInfo) refVal).LineNumber,
                                Message = "Schema definition name should be a resource and not end with \"Value\""
                            });
                            continue;
                        }

                        if (refVal.ToString().EndsWith("Ref"))
                        {
                            result.Add(new RuleCheckResult()
                            {
                                ResultType = RuleCheckResultType.Error,
                                Line = ((IJsonLineInfo) refVal).LineNumber,
                                Message = "Schema definition name should be a resource and not end with \"Ref\""
                            });
                        }
                    }
                }
            }

            return result;
        }
    }
}