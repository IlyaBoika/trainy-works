﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpPut
{
    public class HttpPutResponse204Or200Rules : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();
            var allPutMethodsResponses = input.SelectTokens("$..put.responses").ToArray();
            foreach (var responses in allPutMethodsResponses)
            {
                var response204Or200ExistsInfo = CheckIfResponse204Or200Exists(responses);
                if(response204Or200ExistsInfo.ResultType != RuleCheckResultType.Success)
                {
                    result.Add(response204Or200ExistsInfo);
                    continue;
                }

                var response200ContentHeaderExpectedInfo = CheckExpectedContentTypeHeaderForResponse(responses, "200");
                if (response200ContentHeaderExpectedInfo.ResultType != RuleCheckResultType.Success)
                {
                    result.Add(response200ContentHeaderExpectedInfo);
                }

                var response204ContentHeaderExpectedInfo = CheckExpectedContentTypeHeaderForResponse(responses, "204");
                if (response204ContentHeaderExpectedInfo.ResultType != RuleCheckResultType.Success)
                {
                    result.Add(response204ContentHeaderExpectedInfo);
                }

                var response200LocationHeaderNotExpectedInfo = CheckNotExpectedLocationHeaderForResponse(responses, "200");
                if (response200LocationHeaderNotExpectedInfo.ResultType != RuleCheckResultType.Success)
                {
                    result.Add(response200LocationHeaderNotExpectedInfo);
                }

                var response204LocationHeaderNotExpectedInfo = CheckNotExpectedLocationHeaderForResponse(responses, "204");
                if (response204LocationHeaderNotExpectedInfo.ResultType != RuleCheckResultType.Success)
                {
                    result.Add(response204LocationHeaderNotExpectedInfo);
                }

                var response200SchemaExpectedInfo = CheckExpectedSchemaForResponse(responses, "200");
                if (response200SchemaExpectedInfo.ResultType != RuleCheckResultType.Success)
                {
                    result.Add(response200SchemaExpectedInfo);
                }

                var response204SchemaExpectedInfo = CheckExpectedSchemaForResponse(responses, "204");
                if (response204SchemaExpectedInfo.ResultType != RuleCheckResultType.Success)
                {
                    result.Add(response204SchemaExpectedInfo);
                }

                var schemaDefinitionNameFormatInfo = CheckSchemaDefenitionNameFormatForResponse204(responses);
                if (schemaDefinitionNameFormatInfo.ResultType != RuleCheckResultType.Success)
                {
                    result.Add(schemaDefinitionNameFormatInfo);
                }
            }

            return result;
        }

        private RuleCheckResult CheckIfResponse204Or200Exists(JToken responses)
        {
            var response200 = responses["200"];
            var response204 = responses["204"];
            if(response204 == null && response200 == null)
            {
                return new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Error,
                    Line = ((IJsonLineInfo) responses).LineNumber,
                    Message = "Response 204 or 200 was expected when updating a resource"
                };
            }

            return new RuleCheckResult()
            {
                ResultType = RuleCheckResultType.Success
            };
        }

        private RuleCheckResult CheckExpectedContentTypeHeaderForResponse(JToken responses, string responseCode)
        {
            var response = responses[responseCode];
            var headers = response?["headers"];
            if( headers != null && headers["Content-Type"] == null)
            {
                return new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Warning,
                    Line = ((IJsonLineInfo) headers).LineNumber,
                    Message = "Content-Type header expected when updating a resource"
                };
            }

            return new RuleCheckResult()
            {
                ResultType = RuleCheckResultType.Success
            };
        }

        private RuleCheckResult CheckNotExpectedLocationHeaderForResponse(JToken responses, string responseCode)
        {
            var response = responses[responseCode];
            var headers = response?["headers"];
            if (headers?["Location"] != null)
            {
                return new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Warning,
                    Line = ((IJsonLineInfo) headers["Location"]).LineNumber,
                    Message = "Location header not expected when updating a resource"
                };
            }

            return new RuleCheckResult()
            {
                ResultType = RuleCheckResultType.Success
            };
        }

        private RuleCheckResult CheckExpectedSchemaForResponse(JToken responses, string responseCode)
        {
            var response = responses[responseCode];
            var schema = response?["schema"];
            if (response != null && schema == null)
            {
                return new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Error,
                    Line = ((IJsonLineInfo) response).LineNumber,
                    Message = $"Schema expected for response {responseCode} headers when updating a resource"
                };
            }

            return new RuleCheckResult()
            {
                ResultType = RuleCheckResultType.Success
            };
        }

        private RuleCheckResult CheckSchemaDefenitionNameFormatForResponse204(JToken responses)
        {
            var response = responses["204"];
            var schema = response?["schema"];
            var refName = schema?["$ref"];
            if (refName != null)
            {
                if(refName.ToString().EndsWith("Value"))
                {
                    return new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = ((IJsonLineInfo) refName).LineNumber,
                        Message = "Schema definition name should be a resource and not end with \"Value\""
                    };
                }

                if (refName.ToString().EndsWith("Ref"))
                {
                    return new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = ((IJsonLineInfo) refName).LineNumber,
                        Message = " Schema definition name should be a resource and not end with \"Ref\""
                    };
                }
            }

            return new RuleCheckResult()
            {
                ResultType = RuleCheckResultType.Success
            };
        }
    }
}