﻿using System.Collections.Generic;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpPut
{
    public class HttpPutProducesTypeShouldBeApplicationOrJsonRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();
            var allProducesForPutMethods = input.SelectTokens("$..put.produces");
            foreach (var produces in allProducesForPutMethods)
            {
                if (!produces.ToString().Contains("application/json"))
                {
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = ((IJsonLineInfo) produces).LineNumber,
                        Message = "Application/json should be produced"
                    });
                }
            }

            return result;
        }
    }
}