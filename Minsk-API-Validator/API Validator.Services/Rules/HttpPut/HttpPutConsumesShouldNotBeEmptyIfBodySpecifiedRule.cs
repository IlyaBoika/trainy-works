﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Rules.HttpPut
{
    public class HttpPutConsumesShouldNotBeEmptyIfBodySpecifiedRule : IHttpMethodRule
    {
        public IEnumerable<RuleCheckResult> Check(JObject input)
        {
            var result = new List<RuleCheckResult>();
            var allPutMethods = input.SelectTokens("$..put");
            foreach(var method in allPutMethods)
            {
                if(method == null)
                {
                    continue;
                }

                var parameters = method["parameters"]?.Children().ToArray();
                if (parameters == null)
                {
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = ((IJsonLineInfo)method).LineNumber,
                        Message = $"Rule check fail: \"parameters\" not found for path: {method.Path}/parameters"
                    });
                    continue;
                }

                var hasBody = false;
                foreach(var parameter in parameters)
                {
                    if(parameter["in"].ToString() == "body")
                    {
                        hasBody = true;
                        break;
                    }
                }

                if(hasBody && !method["consumes"].ToString().Contains("application/json"))
                {
                    result.Add(new RuleCheckResult()
                    {
                        ResultType = RuleCheckResultType.Error,
                        Line = ((IJsonLineInfo) method["consumes"]).LineNumber,
                        Message = "Consumes should not be empty if a body parameter is specified"
                    });
                }
            }

            return result;
        }
    }
}