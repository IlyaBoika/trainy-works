﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;

namespace API_Validator.Services
{
    public class ValidationService : IValidationService
    {
        private readonly IApiConsistencyValidator _apiConsistencyValidator;
        private readonly ICustomRuleValidator _customRuleValidator;
        private readonly IHttpMethodValidator _httpMethodValidator;
        private readonly IErrorSorter _errorSorter;

        public ValidationService(IApiConsistencyValidator apiConsistencyValidator, IHttpMethodValidator httpMethodValidator, IErrorSorter errorSorter, ICustomRuleValidator customRuleValidator)
        {
            _apiConsistencyValidator = apiConsistencyValidator;
            _httpMethodValidator = httpMethodValidator;
            _errorSorter = errorSorter;
            _customRuleValidator = customRuleValidator;
        }

        public IEnumerable<RuleCheckResult> Verify(string input)
        {
            var consistencyErrors = _apiConsistencyValidator.Validate(input);
            //Some code here

            if (consistencyErrors.Any())
            {
                var sortedConsistencyErrors = _errorSorter.SortByErrorLine(consistencyErrors);

                return sortedConsistencyErrors;
            }

            var resultFromHttpRules = _httpMethodValidator.Validate(input);

            var resultFromCustomRules = _customRuleValidator.Validate(input);

            var resultErrorList = new List<RuleCheckResult>();

            if (resultFromHttpRules.Any())
            {
                resultErrorList.AddRange(resultFromHttpRules);
            }

            if (resultFromCustomRules.Any())
            {
                resultErrorList.AddRange(resultFromCustomRules);
            }

            if (!(resultErrorList.Any()))
            {
                resultErrorList.Add(new RuleCheckResult()
                {
                    ResultType = RuleCheckResultType.Success,
                    Message = "There are no errors or warnings in the API-instruction"
                });

                return resultErrorList;
            }

            var sortedResultFromHttpAndCustomRules = _errorSorter.SortByErrorLine(resultErrorList);

            return sortedResultFromHttpAndCustomRules;
        }
    }
}