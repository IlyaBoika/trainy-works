﻿using System;
using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.enums;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;

namespace API_Validator.Services
{
    public class RuleManager : IRuleManager
    {
        private readonly Dictionary<string, HttpRuleModel> _httpDeleteRules;
        private readonly Dictionary<string, HttpRuleModel> _httpGetRules;
        private readonly Dictionary<string, HttpRuleModel> _httpHeadRules;
        private readonly Dictionary<string, HttpRuleModel> _httpPostRules;
        private readonly Dictionary<string, HttpRuleModel> _httpPutRules;
        private readonly Dictionary<string, CustomRuleModel> _customRules;


        public RuleManager(IHttpMethodRuleFactory httpMethodRuleFactory, ICustomRuleFactory customRuleFactory)
        {
            _httpDeleteRules = httpMethodRuleFactory.CreateDeleteMethodRules()
                .ToDictionary(p => p.Description.RuleClassName, p => p);
            _httpGetRules = httpMethodRuleFactory.CreateGetMethodRules()
                .ToDictionary(p => p.Description.RuleClassName, p => p);
            _httpHeadRules = httpMethodRuleFactory.CreateHeadMethodRules()
                .ToDictionary(p => p.Description.RuleClassName, p => p);
            _httpPostRules = httpMethodRuleFactory.CreatePostMethodRules()
                .ToDictionary(p => p.Description.RuleClassName, p => p);
            _httpPutRules = httpMethodRuleFactory.CreatePutMethodRules()
                .ToDictionary(p => p.Description.RuleClassName, p => p);

            _customRules = customRuleFactory.CreateCustomRules()
                .ToDictionary(p => p.Description.RuleClassName, p => p);
        }


        public IEnumerable<RuleDescription> GetAllHttpRuleDescriptions(HttpMethodType type)
        {
            switch(type)
            {
                case HttpMethodType.Get :
                {
                    return _httpGetRules.Values.Select(p => p.Description);
                }
                case HttpMethodType.Post :
                {
                    return _httpPostRules.Values.Select(p => p.Description);
                }
                case HttpMethodType.Put :
                {
                    return _httpPutRules.Values.Select(p => p.Description);
                }
                case HttpMethodType.Delete :
                {
                    return _httpDeleteRules.Values.Select(p => p.Description);
                }
                case HttpMethodType.Head :
                {
                    return _httpHeadRules.Values.Select(p => p.Description);
                }

                default: throw new ArgumentException("Wrong type",nameof(type));
            }
        }

        public IEnumerable<RuleDescription> GetAllCustomRuleDescriptions()
        {
            return _customRules.Values.Select(p => p.Description);
        }

        public void SetAllRuleStates(bool state)
        {
            foreach(var rule in _httpGetRules.Values)
            {
                rule.Description.IsSelected = state;
            }

            foreach (var rule in _httpPostRules.Values)
            {
                rule.Description.IsSelected = state;
            }

            foreach (var rule in _httpPutRules.Values)
            {
                rule.Description.IsSelected = state;
            }

            foreach (var rule in _httpDeleteRules.Values)
            {
                rule.Description.IsSelected = state;
            }

            foreach (var rule in _httpHeadRules.Values)
            {
                rule.Description.IsSelected = state;
            }

            foreach (var rule in _customRules.Values)
            {
                rule.Description.IsSelected = state;
            }
        }

        public void SetRuleStateByName(string name, bool state)
        {
            if (name.StartsWith("Http"))
            {
                HttpRuleModel rule = null;
                if (name.StartsWith("HttpGet"))
                {
                    _httpGetRules.TryGetValue(name,out rule);
                }
                else if (name.StartsWith("HttpPost"))
                {
                    _httpPostRules.TryGetValue(name, out rule);
                }
                else if (name.StartsWith("HttpPut"))
                {
                    _httpPutRules.TryGetValue(name, out rule);
                }
                else if (name.StartsWith("HttpDelete"))
                {
                    _httpDeleteRules.TryGetValue(name, out rule);
                }
                else if (name.StartsWith("HttpHead"))
                {
                    _httpHeadRules.TryGetValue(name, out rule);
                }

                if(rule != null)
                {
                    rule.Description.IsSelected = state;
                    return;
                }
            }

            _customRules.TryGetValue(name, out CustomRuleModel customRule);
            if (customRule != null)
            {
                customRule.Description.IsSelected = state;
                return;
            }

            throw new ArgumentException($"Rule with name {name} not found", nameof(name));
        }

        public IEnumerable<IHttpMethodRule> GetHttpRules(HttpMethodType type)
        {
            switch (type)
            {
                case HttpMethodType.Get:
                {
                    return _httpGetRules.Values.Where(p => p.Description.IsSelected).Select(p => p.Rule);
                }
                case HttpMethodType.Post:
                {
                    return _httpPostRules.Values.Where(p => p.Description.IsSelected).Select(p => p.Rule);
                }
                case HttpMethodType.Put:
                {
                    return _httpPutRules.Values.Where(p => p.Description.IsSelected).Select(p => p.Rule);
                }
                case HttpMethodType.Delete:
                {
                    return _httpDeleteRules.Values.Where(p => p.Description.IsSelected).Select(p => p.Rule);
                }
                case HttpMethodType.Head:
                {
                    return _httpHeadRules.Values.Where(p => p.Description.IsSelected).Select(p => p.Rule);
                }

                default: throw new ArgumentException("Wrong type", nameof(type));
            }
        }

        public IEnumerable<ICustomRule> GetCustomRules()
        {
            return _customRules.Values.Where(p => p.Description.IsSelected).Select(p => p.Rule);
        }
    }
}