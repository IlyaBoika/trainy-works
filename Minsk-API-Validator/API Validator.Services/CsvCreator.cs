﻿using System.Collections.Generic;
using System.Text;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;

namespace API_Validator.Services
{
    public class CsvCreator : ICsvCreator
    {
        public string Create(IEnumerable<RuleCheckResult> ruleCheckResults)
        {
            var msg = new StringBuilder();

            msg.Append("Line, Type, Message\n");

            foreach (var item in ruleCheckResults)
            {
                var line = item.Line == null ? "Null" : item.Line.ToString();
                msg.Append($"{line},{item.ResultType},{item.Message}\n");
            }

            return msg.ToString();
        }
    }
}