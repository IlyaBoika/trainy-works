﻿using API_Validator.Services.Interfaces;

namespace API_Validator.Services.Models
{
    public class CustomRuleModel
    {
        public ICustomRule Rule { get; set; }
        public RuleDescription Description { get; set; }
    }
}