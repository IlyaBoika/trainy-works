﻿using System.Text;
using API_Validator.Services.enums;

namespace API_Validator.Services.Models
{
    public class RuleCheckResult
    {
        public RuleCheckResultType ResultType { get; set; }
        public int? Line { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            if(ResultType == RuleCheckResultType.Success)
            {
                result.Append($"Success");
            }

            if(Line != null)
            {
                result.AppendLine($"Line {Line}");
            }

            if((int) ResultType > 0 && ResultType != RuleCheckResultType.Success)
            {
                result.Append($": {ResultType.ToString()}");
            }

            if(Message != null && Line != null)
            {
                result.Append($" - {Message}");
            }
            else if(Message != null && Line == null)
            {
                result.Append($" {Message}");
            }

            return result.ToString();
        }
    }
}