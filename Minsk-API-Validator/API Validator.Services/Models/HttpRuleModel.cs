﻿using API_Validator.Services.Interfaces;

namespace API_Validator.Services.Models
{
    public class HttpRuleModel
    {
        public IHttpMethodRule Rule { get; set; }
        public RuleDescription Description { get; set; }
    }
}