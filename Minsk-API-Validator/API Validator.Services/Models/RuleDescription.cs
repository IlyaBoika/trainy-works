﻿namespace API_Validator.Services.Models
{
    public class RuleDescription
    {
        public string RuleClassName { get; set; }
        public string RuleName { get; set; }
        public string RuleDescriptionInfo { get; set; }
        public bool IsSelected { get; set; }
    }
}