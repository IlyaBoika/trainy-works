﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Syncfusion.Drawing;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf.Graphics;

namespace API_Validator.Services
{
    public class PdfCreator : IPdfCreator
    {
        private readonly string _contentRootPath;

        public PdfCreator()
        {
            _contentRootPath = Environment.CurrentDirectory;
        }

        public MemoryStream Create(IEnumerable<RuleCheckResult> ruleCheckResults)
        {
            var htmlInfo = new StringBuilder();
            htmlInfo.AppendLine(
                "<style>table { border-collapse: collapse; width: 100%;} th, td { text-align: left; padding: 8px;} tr:nth-child(even) {background-color: #f2f2f2;} </style>");
            htmlInfo.AppendLine("<table class=\"table table - hover\">");
            htmlInfo.AppendLine("<thead><tr><td>Line</td><td>Type</td><td>Message</td></tr></thead>");
            htmlInfo.AppendLine("<tbody>");
            foreach (var item in ruleCheckResults)
            {
                htmlInfo.AppendLine($"<tr><td>{item.Line}</td><td>{item.ResultType}</td><td>{item.Message}</td></tr>");
            }

            htmlInfo.AppendLine("</tbody>");
            htmlInfo.AppendLine("</table>");


            var htmlConverter = new HtmlToPdfConverter();
            var settings = new WebKitConverterSettings
            {
                WebKitPath = Path.Combine(_contentRootPath, "QtBinariesWindows"),
                Margin = new PdfMargins() {Left = 30, Top = 20, Right = 30, Bottom = 20},
                PdfPageSize = new SizeF(595, 842)
            };
            htmlConverter.ConverterSettings = settings;

            var document = htmlConverter.Convert(htmlInfo.ToString(), "vk.com");
            MemoryStream stream = new MemoryStream();
            document.Save(stream);
            return stream;
        }
    }
}