﻿using System.Collections.Generic;
using API_Validator.Services.Models;

namespace API_Validator.Services.Interfaces
{
    public interface IHttpMethodRuleFactory
    {
        IEnumerable<HttpRuleModel> CreateDeleteMethodRules();
        IEnumerable<HttpRuleModel> CreateGetMethodRules();
        IEnumerable<HttpRuleModel> CreateHeadMethodRules();
        IEnumerable<HttpRuleModel> CreatePutMethodRules();
        IEnumerable<HttpRuleModel> CreatePostMethodRules();
    }
}