﻿using System.Collections.Generic;
using API_Validator.Services.Models;

namespace API_Validator.Services.Interfaces
{
    public interface IValidationService
    {
        IEnumerable<RuleCheckResult> Verify(string input);
    }
}