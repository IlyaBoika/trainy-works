﻿using System.Collections.Generic;
using API_Validator.Services.Models;
using Newtonsoft.Json.Linq;

namespace API_Validator.Services.Interfaces
{
    public interface ICustomRule
    {
        int Id { get; set; }
        IEnumerable<RuleCheckResult> Check(JObject input);
    }
}