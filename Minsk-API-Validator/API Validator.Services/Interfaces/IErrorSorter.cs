﻿using System.Collections.Generic;
using API_Validator.Services.Models;

namespace API_Validator.Services.Interfaces
{
    public interface IErrorSorter
    {
        IEnumerable<RuleCheckResult> SortByErrorLine(IEnumerable<RuleCheckResult> input);
    }
}