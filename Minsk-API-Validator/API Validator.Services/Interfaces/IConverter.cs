﻿namespace API_Validator.Services.Interfaces
{
    public interface IConverter<T>
    {
        string Convert(T input);
    }
}