﻿using System.Collections.Generic;
using API_Validator.Services.enums;
using API_Validator.Services.Models;

namespace API_Validator.Services.Interfaces
{
    public interface IRuleManager
    {
        IEnumerable<RuleDescription> GetAllHttpRuleDescriptions(HttpMethodType type);
        IEnumerable<RuleDescription> GetAllCustomRuleDescriptions();
        void SetAllRuleStates(bool state);
        void SetRuleStateByName(string name, bool state);
        IEnumerable<IHttpMethodRule> GetHttpRules(HttpMethodType type);
        IEnumerable<ICustomRule> GetCustomRules();
    }
}