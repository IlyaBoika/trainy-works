﻿using System.Collections.Generic;
using API_Validator.Services.Models;

namespace API_Validator.Services.Interfaces
{
    public interface IApiConsistencyValidator
    {
        IEnumerable<RuleCheckResult> Validate(string inputApi);
    }
}