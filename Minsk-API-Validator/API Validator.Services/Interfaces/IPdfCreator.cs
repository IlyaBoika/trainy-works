﻿using System.Collections.Generic;
using System.IO;
using API_Validator.Services.Models;

namespace API_Validator.Services.Interfaces
{
    public interface IPdfCreator
    {
        MemoryStream Create(IEnumerable<RuleCheckResult> ruleCheckResults);
    }
}