﻿namespace API_Validator.Services.enums
{
    public enum RuleCheckResultType
    {
        Error = 1,
        Warning,
        Success
    }
}