﻿namespace API_Validator.Services.enums
{
    public enum HttpMethodType
    {
        Get,
        Post,
        Put,
        Delete,
        Head
    }
}