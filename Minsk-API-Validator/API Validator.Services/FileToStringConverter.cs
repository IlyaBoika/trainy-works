﻿using API_Validator.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Text;

namespace API_Validator.Services
{
    public class FileToStringConverter : IConverter<IFormFile>
    {
        public string Convert(IFormFile file)
        {
            var result = new StringBuilder();

            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                while (reader.Peek() >= 0)
                    result.AppendLine(reader.ReadLine());
            }

            var resultInString = result.ToString();

            return resultInString;
        }
    }
}