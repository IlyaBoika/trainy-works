﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;

namespace API_Validator.Services
{
    public class ErrorSorter : IErrorSorter
    {
        public IEnumerable<RuleCheckResult> SortByErrorLine(IEnumerable<RuleCheckResult> input)
        {
            var list = new List<RuleCheckResult>();
            var incorrectList = new List<RuleCheckResult>();
            var result = new List<RuleCheckResult>(input.Count());
            foreach (var line in input)
            {
                if (line.Line != null)
                {
                    list.Add((line));
                }
                else
                {
                    incorrectList.Add(line);
                }
            }

            result.AddRange(list.OrderBy(p => p.Line));
            result.AddRange(incorrectList);
            return result;
        }
    }
}