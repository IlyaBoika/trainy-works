﻿namespace API_Validator.Services
{
    public static class JsonSchema
    {
        public static string Schema = @"{
    ""$schema"": ""schema"",
    ""$ref"": ""#/definitions/API"",
    ""definitions"": {
        ""API"": {
            ""type"": ""object"",
            ""additionalProperties"": true,
            ""properties"": {
                ""info"": {
                    ""type"": ""object""
                },
                ""paths"": {
                    ""type"": ""object""
                },
            },
            ""required"": [
                ""info"",
                ""paths"",
            ],
            ""title"": ""API""
        }
    }
}";
    }
}