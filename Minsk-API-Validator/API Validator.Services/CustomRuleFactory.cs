﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using API_Validator.Services.Rules.CustomRules;

namespace API_Validator.Services
{
    public class CustomRuleFactory : ICustomRuleFactory
    {
        public IEnumerable<CustomRuleModel> CreateCustomRules()
        {
            var customRules = new List<ICustomRule>
            {
                new AlphanumericRule(),
            };

            var result = customRules.Select(p => new CustomRuleModel() { Rule = p, Description = GetDescription(p) });

            return result;
        }

        private RuleDescription GetDescription(ICustomRule rule)
        {
            var className = rule.GetType().Name;
            return new RuleDescription()
            {
                RuleClassName = className,
                RuleDescriptionInfo = className,
                RuleName = className,
                IsSelected = true
            };
        }
    }
}