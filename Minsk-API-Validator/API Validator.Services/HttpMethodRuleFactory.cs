﻿using System.Collections.Generic;
using System.Linq;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using API_Validator.Services.Rules.HttpDelete;
using API_Validator.Services.Rules.HttpGet;
using API_Validator.Services.Rules.HttpHead;
using API_Validator.Services.Rules.HttpPost;
using API_Validator.Services.Rules.HttpPut;

namespace API_Validator.Services
{
    public class HttpMethodRuleFactory : IHttpMethodRuleFactory
    {
        public IEnumerable<HttpRuleModel> CreateDeleteMethodRules()
        {
            var deleteMethodRules = new List<IHttpMethodRule>
            {
                new HttpDeleteConsumesShouldBeEmptyRule(),
                new HttpDeleteOperationIdFormatRule(),
                new HttpDeleteProducesNotEqualToZeroRule(),
                new HttpDeleteResponse204HeaderExistRule(),
                new HttpDeleteResponse204NotExistRule(),
                new HttpDeleteResponse204SchemaExpectedRule()
            };
            var result = deleteMethodRules.Select(p => new HttpRuleModel() { Rule = p, Description = GetDescription(p) });

            return result;
        }

        public IEnumerable<HttpRuleModel> CreateGetMethodRules()
        {
            var getMethodRules = new List<IHttpMethodRule>
            {
                new HttpGetCode200ExpectedRule(),
                new HttpGetConsumesShouldBeEmptyRule(),
                new HttpGetHeaderOfContentTypeExpectedRule(),
                new HttpGetOperationIdFormatRule(),
                new HttpGetProducesNotApplicationOrJsonTypeRule(),
                new HttpGetSchemaDefenitionNameRule(),
                new HttpGetSchemaExpectedRule()
            };

            var result = getMethodRules.Select(p => new HttpRuleModel() { Rule = p, Description = GetDescription(p) });

            return result;
        }

        public IEnumerable<HttpRuleModel> CreateHeadMethodRules()
        {
            var headMethodRules = new List<IHttpMethodRule>
            {
                new HttpHeadConsumesShouldBeEmptyRule(),
                new HttpHeadOperationIdFormatRule(),
                new HttpHeadProducesShouldBeEmptyRule(),
                new HttpHeadResponse200ExpectedRule(),
                new HttpHeadResponse200SchemaAndHeaderRule(),
            };

            var result = headMethodRules.Select(p => new HttpRuleModel() { Rule = p, Description = GetDescription(p) });


            return result;
        }

        public IEnumerable<HttpRuleModel> CreatePostMethodRules()
        {
            var postMethodRules = new List<IHttpMethodRule>
            {
                new HttpPostApplicationOrJsonShouldBeProducedIfAcceptHeaderSpecifiedRule(),
                new HttpPostConsumesShouldNotBeEmptyIfBodySpecifiedRule(),
                new HttpPostOperationIdFormatRule(),
                new HttpPostParametersSchemaFormatRule(),
                new HttpPostResponse201Rules(),
            };

            var result = postMethodRules.Select(p => new HttpRuleModel() { Rule = p, Description = GetDescription(p) });

            return result;
        }

        public IEnumerable<HttpRuleModel> CreatePutMethodRules()
        {
            var putMethodRules = new List<IHttpMethodRule>
            {
                new HttpPutConsumesShouldNotBeEmptyIfBodySpecifiedRule(),
                new HttpPutOperationIdFormatRule(),
                new HttpPutParametersSchemaDefenitionNameRule(),
                new HttpPutParametersSchemaDefenitionNameShouldBeResourceCollectionRule(),
                new HttpPutProducesTypeShouldBeApplicationOrJsonRule(),
                new HttpPutResponse204Or200Rules(),
            };

            var result = putMethodRules.Select(p => new HttpRuleModel() { Rule = p, Description = GetDescription(p) });

            return result;
        }

        private RuleDescription GetDescription(IHttpMethodRule rule)
        {
            var className = rule.GetType().Name;
            return new RuleDescription()
            {
                RuleClassName = className,
                RuleDescriptionInfo = className,
                RuleName = className,
                IsSelected = true
            };
        }
    }
}