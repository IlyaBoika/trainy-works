Для связи клиентской части приложения и серверной существует много различных способов(gRPC - удаленный вызов процедур, GraphQL язык запросов разработанный FaceBook, tcpClient & tcpListener, UdpClient и др.). Среди них наиболее популярным является REST API (representational state Transfer API). Данный подход к разработке является общим подходом представления сервисов окружающему миру. Причина его популярности заключается в простоте, легкости использования и доступе через HTTP.
    Swagger является спецификацией REST API. он описывает интерфейс взаимодействия с веб сервисом в виде JSON файлов. в данных файлах описаны все URl-пути для взаимодействия с приложением, ожидаемые операции для каждого пути (GET, POST...), ожидаемые параметры для каждого пути, ожидаемые ответы, а также типы этих парметров и ответов. Данные Json-файлы генерируются автоматически на основании анализа кода приложения.
    Перед нами была поставлена задача реализовать веб приложение  которое молго бы анализировать эти сгенерированные файлы, проверять их на соответствие best practicies, и выдавать информацию о результатах проверки. 
    Данное приложение предполагается использовать на этапе разработки API для самоконтроля.

 В приложении есть встроенные правила для проверки таких json-файлов на соответствие заданным критериям. Имеется возможность производить настройку, по каким из них будет производится проверка. Ниже перечислены все правила, по умолчанию встроенные в приложение и их краткое описание.

Общие правила:
1. AlphanumericRule - проверка на то, чтобы все ссылки внутри файла были только цифробуквенными.

Правила проверки HTTP методов:

2.HTTP DELETE

2.1 HttpDeleteResponse204NotExistRule - проверка на наличие ответа 204 при удалении ресурса:
if (Responses.notexist("204")))
        ERROR "204 expected when deleting a resource"

2.2 HttpDeleteResponse204HeaderExistRule - проверка на отсуствие возвращения Content-Type при удалении ресурса:
if (Responses.204.Headers.exist("Content-Type"))   
        ERROR "Content-Type should not be returned when deleting a resource" 

2.3 HttpDeleteResponse204SchemaExpectedRule - проверка на отсуствие возвращения Schema  при удалении ресурса:
if (Responses.204.Exist("schema") && Responses.204.schema.Exist("$ref")) 
        ERROR "No Schema expected when deleting a resource"

2.4 HttpDeleteProducesNotEqualToZeroRule - проверка на отсуствие возвращения produces при удалении ресурса:
if length(produces)>0:
        ERROR  "Should not produce any content-types when deleting a resource: %s'%path

2.5 HttpDeleteConsumesShouldBeEmptyRule - проверка на отсуствие возвращения consumes при удалении ресурса:
if len(consumes) > 0:
         ERROR "consumes should be empty when deleting a resource: %s"%path

2.6. HttpDeleteOperationIdFormatRule - проверка на формат OperationId:
if path.endswith('}'):
        if len(tags)==0:
            'ERROR', 'Add resource name to tag array: %s'%path
            else:
                res_name = (tags)[0]
                op = (operationId)
                if not op.startswith('Delete') and not op.lower().endswith(res_name):
                    'ERROR', 'OperationId should start with "Delete" and end with "{0}": {1}'.format(res_name, op)

3.HTTP GET

3.1 HttpGetCode200ExpectedRule - проверка на наличие ответа 200 при возвращении ресурса:
if not "200" in (responses):
        'ERROR', '200 expected when returning a resource: %s'%path

3.2 HttpGetHeaderOfContentTypeExpectedRule - проверка на наличие  Content-Type при возвращении ресурса:
if not "Content-Type" in (responses.200.headers):
        'ERROR', 'Content-Type header expected when returning a resource: %s'%path

3.3 HttpGetSchemaExpectedRule - проверка на наличие Schema при возвращении ресурса:
  if not "$ref" in (responses.200.schema):
        'ERROR', 'Schema expected when returning a resource: %s'%path

3.4 HttpGetSchemaDefenitionNameRule - проверка на формат контента Schema:
val = data.get_value(responses.200.schema.$ref)
        if val.endswith('Value') or val.endswith('Ref'):
            'ERROR', 'Schema definition name should be a resource and not end with "Value" or "Ref": %s'%path

val = data.get_value(responses.200.schema.$ref)
            if not val.endswith("Collection"):
                'WARN', 'Schema definition name should be a resource collection, suffixed with "Collection": %s'%path
            elif val.endswith('ValueCollection') or val.endswith('RefCollection'):
                'WARN', 'Schema definition name should be a resource collection and not end with "ValueCollection" or "RefCollection": %s'%path
    yield 'OK'

3.5 HttpGetProducesNotApplicationOrJsonTypeRule - проверка желательного псевдонима application/json в produces при возвращении ресурса:
if not "application/json" in data.get_value(produces):
        'WARN', 'application/json is typical mime type when returning a resource: %s'%path
    yield 'OK'

3.6 HttpGetConsumesShouldBeEmptyRule - проверка на отсуствие consumes при возвращении ресурса:
def consumes_should_be_empty(data):
    if len(data.get_value(consumes)) > 0:
        yield 'ERROR', 'consumes should be empty when returning a resource: %s'%path
    yield 'OK'

3.7 HttpGetOperationIdFormatRule - проверка на формат  OperationId:
res_name = data.get_value('%s.responses.200.schema.$ref'%path).split('/')[-1]
    op = data.get_value(operationId)
        if not op.startswith('Get') and not op.endswith(res_name):
            yield 'ERROR', 'OperationId should start with "Get" and end with "{0}": {1}'.format(res_name, path)
    yield 'OK'

res_name = data.get_value(responses.200.schema.$ref).split('/')[-1]
    op = ['operationId']
    if not op.endswith(res_name):
       'ERROR', 'OperationId should end with "{0}": {1}'.format(res_name, path)
    yield 'OK'

4.HttpHead

4.1 HttpHeadResponse200ExpectedRule - проверка на наличие ответа 200:
if not "200" in data.get_value(responses):
        'ERROR', '200 expected when head: %s'%path

4.2 HttpHeadResponse200SchemaAndHeaderRule - проверка на отсуствие наличия в responses.200 Schema и Content-Type:
if "headers" in data.get_value(responses.200):
            if "Content-Type" in data.get_value(responses.200.headers):
                'ERROR', 'Content-Type header not expected when head: %s'%path

_200 = data.get_value(responses.200)
        if "schema" in _200 
           'ERROR', 'Schema not expected when head: %s'%path
    yield 'OK'

4.3 HttpHeadProducesShouldBeEmptyRule - проверка на отсуствие produces в ответе:
if len(data.get_value(produces)>0:
        'ERROR', 'produces shoud be empty when head: %s'%path
    yield 'OK

4.4 HttpHeadProducesShouldBeEmptyRule - проверка на отсуствие consumes в ответе:
if len(data.get_value('%s.consumes'%path)) > 0:
    'ERROR', 'consumes should be empty when head: %s'%path
    yield 'OK'

4.5 HttpHeadOperationIdFormatRule - проверка на формат  OperationId:
op = data.get_value('%s.operationId'%path)
        if not op.endswith("Exists"):
        'ERROR', 'OperationId should end with "Exists": {0}'.format(path)
    yield 'OK'

5.HttpPost

5.1 HttpPostResponse201Rules - проверка на формат ответа 201:
if not "201" in data.get_value(responses):
         'ERROR', '201 expected when creating a resource: '+path
    yield 'OK'

if "Content-Type" in data.get_value(responses.201.headers):
        'WARN', 'Content-Type header not expected when creating a resource: %s'%path
    yield 'OK'

if not "Location" in data.get_value("%s.responses.201.headers"%path):
       yield 'WARN', 'Location header expected when creating a resource: %s'%path
    yield 'OK

p = data.get_value('%s.responses.201'%path)
        if "schema" in p 
            yield 'ERROR', 'respone schema not expected when creating a resource: %s'%path
    yield 'OK'

5.2 HttpPostParametersSchemaFormatRule - проверка на формат Schema в ответе 201:
p = data.get_value('%s.responses.201'%path)
        if "schema" in p 
            yield 'ERROR', 'respone schema not expected when creating a resource: %s'%path
    yield 'OK'

for p in data.get_value('%s.parameters'%path):
            if p['in'] == 'body' and "schema" in p:
                val = p['schema']['$ref']
                if val.endswith('Value') or val.endswith('Ref'):
                    yield 'ERROR', 'Schema definition name should be a resource and not end with "Value" or "Ref": %s'%path
    yield 'OK'

5.3 HttpPostApplicationOrJsonShouldBeProducedIfAcceptHeaderSpecifiedRule - проверка на формат application/json:
appjson = "application/json" in data.get_value('%s.produces'%path)
        hasAccepet_value('%s.parameters'%path):
            if p['in'] == "header" and p['name'] == 'Accept' 
                {hasAcceptHeader = True}
        if appjson and not hasAcceptHeader:
            'WARN', 'application/json should only be produced if an accept header is specified: %s'%path
    yield 'OK

5.4 HttpPostConsumesShouldNotBeEmptyIfBodySpecifiedRule - проверка на отсуствие  consumes в ответе:
hasBody = False
        for p in data.get_value('%s.parameters'%path):
            if p['in'] == "body" 
                hasBody = True
        if hasBody and len(data.get_value('%s.consumes'%path)) == 0:
            'ERROR', 'consumes should not be empty if a body parameter is specified: %s'%path
    yield 'OK'

5.5 HttpPostOperationIdFormatRule - проверка на формат OperationId:
res_name = None
        for p in data.get_value('%s.parameters'%path):
            if p['in'] == 'body' and "schema" in p:
                res_name = p['schema']['$ref'].split('/')[-1]
        op = data.get_value('%s.operationId'%path)
        if not op.startswith('Create'):
            'ERROR', 'OperationId should start with "Create": {1}'.format(path)
        if not res_name == None and not op.endswith(res_name) :
            'ERROR', 'OperationId should end with "{0}": {1}'.format(res_name, path)
    yield 'OK'

6.HttpPut

6.1 HttpPutResponse204Or200Rules - проверка на формат ответа 200 и 204:
resp = data.get_value("%s.responses"%path)
        if not "200" in resp and not "204" in resp:
            'ERROR', '200 or 204 expected when updating a resource: %s'%path
    yield 'OK'

for code in ['200','204']:
            if code in data.get_value("%s.responses"%path):
                if not "Content-Type" in data.get_value("%s.responses.%s.headers"% (path, code)):
                    'WARN', 'Content-Type header expected when updating a resource: %s'%path
    yield 'OK'

for code in ['200','204']:
            if code in data.get_value("%s.responses"%path):
                if "Location" in data.get_value("%s.responses.%s.headers"% (path, code)):
                    'WARN', 'Location header not expected when updating a resource: %s'%path
    yield 'OK'

6.2 HttpPutParametersSchemaDefenitionNameRule - проверка на наличие schema в ответах 200 и 204:
if "200" in data.get_value("%s.responses"%path):
            p = data.get_value('%s.responses.200'%path)
            if not p['schema']:
                'ERROR', 'response schema expected when updating a resource: %s'%path
    yield 'OK'

if "204" in data.get_value("%s.responses"%path):
            p = data.get_value('%s.responses.204'%path)
            if not p['schema']:
                'ERROR', 'response schema expected when updating a resource: %s'%path
    yield 'OK'

6.3 HttpPutParametersSchemaDefenitionNameShouldBeResourceCollectionRule - проверка на формат имен schema в ответах 200 и 204:

        if "200" in data.get_value('%s.responses'%path):
            p = data.get_value('%s.responses.200'%path)
            if "schema" in p and "$ref" in p['schema']:
                val = p['schema']['$ref']
                if val.endswith('Value') or val.endswith('Ref'):
                    yield 'ERROR', 'Schema definition name should be a resource and not end with "Value" or "Ref": %s'%path
    yield 'OK'

for p in data.get_value('%s.parameters'%path):
            if p['in'] == 'body' and "schema" in p:
                val = p['schema']['$ref']
                if val.endswith('Value') or val.endswith('Ref'):
                    yield 'ERROR', 'Schema definition name should be a resource and not end with "Value" or "Ref": %s'%path
    yield 'OK'

if re.match('.*[a-z]$',url_path):
            for p in data.get_value('%s.parameters'%path):
                if p['in'] == 'body' and "schema" in p:
                    val = p['schema']['$ref']
                    if not val.endswith("Collection"):
                        yield 'WARN', 'Schema definition name should be a resource collection, suffixed with "Collection": %s'%path
                    elif val.endswith('ValueCollection') or val.endswith('RefCollection'):
                        yield 'WARN', 'Schema definition name should be a resource collection and not end with "ValueCollection" or "RefCollection": %s'%path
    yield 'OK'

6.4 HttpPutProducesTypeShouldBeApplicationOrJsonRule - проверка на наличие  application/json в produces:
if not "application/json" in data.get_value('%s.produces'%path):
            yield 'WARN', 'application/json should be produced: %s'%path
    yield 'OK'

6.5 HttpPutConsumesShouldNotBeEmptyIfBodySpecifiedRule - проверка consumes на наличие контента если указан body параметр:
hasBody = False
        for p in data.get_value('%s.parameters'%path):
            if p['in'] == "body": hasBody = True
        if hasBody and not "application/json" in data.get_value('%s.consumes'%path):
            yield 'ERROR', 'consumes should not be empty if a body parameter is specified: %s'%path
    yield 'OK'

6.6 HttpPutOperationIdFormatRule - проверка на формат OperationId:
 res_name = None
        for p in data.get_value('%s.parameters'%path):
            if p['in'] == 'body' and "schema" in p:
                res_name = p['schema']['$ref'].split('/')[-1]
        op = data.get_value('%s.operationId'%path)
        if not op.startswith('Update'):
            yield 'ERROR', 'OperationId should start with "Update": {0}'.format(path)
        if not res_name == None and not op.endswith(res_name) :
            yield 'ERROR', 'OperationId should end with "{0}": {1}'.format(res_name, path)
    yield 'OK'