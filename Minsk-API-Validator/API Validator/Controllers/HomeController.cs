﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using API_Validator.Models;
using Microsoft.AspNetCore.Mvc;
using API_Validator.Services.enums;
using Microsoft.AspNetCore.Http;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Models;
using Newtonsoft.Json;

namespace API_Validator.Controllers
{
    public class HomeController : Controller
    {
        private readonly IValidationService _service;
        private readonly IConverter<IFormFile> _converter;
        private readonly IPdfCreator _pdfCreator;
        private readonly ICsvCreator _csvCreator;
        private readonly IRuleManager _ruleManager;

        private readonly RulesDescriptionViewModel _rulesDescriptionVm;

        public HomeController(IValidationService service, IConverter<IFormFile> converter, IPdfCreator pdfCreator, ICsvCreator csvCreator, IRuleManager ruleManager)
        {
            _service = service;
            _converter = converter;
            _pdfCreator = pdfCreator;
            _csvCreator = csvCreator;
            _ruleManager = ruleManager;
            _rulesDescriptionVm = new RulesDescriptionViewModel();
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Validate(IFormFile validatedFile)
        {
            if (validatedFile == null)
            {
                return RedirectToAction("Index");
            }

            if (TempData.ContainsKey("save"))
            {
                var rules = (string[]) TempData.Peek("save");
                _ruleManager.SetAllRuleStates(false);
                if (rules != null)
                {
                    try
                    {
                        foreach (var rule in rules)
                        {
                            if (rule != null)
                            {
                                _ruleManager.SetRuleStateByName(rule, true);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }

            IEnumerable<RuleCheckResult> validationResult;

            var convertedInput = _converter.Convert(validatedFile);
            validationResult = _service.Verify(convertedInput);

            var model = new ValidationResultsViewModel
            {
                ValidationResult = validationResult,
                Filename = validatedFile.FileName,
            };

            return View("~/Views/Home/Index.cshtml", model);
        }


        [HttpGet]
        public IActionResult SetRules()
        {
            if (TempData.ContainsKey("save"))
            {
                var rules = (string[]) TempData.Peek("save");
                _ruleManager.SetAllRuleStates(false);
                if(rules != null)
                {
                    try
                    {
                        foreach (var rule in rules)
                        {
                            if (rule != null)
                            {
                                _ruleManager.SetRuleStateByName(rule, true);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }

            _rulesDescriptionVm.HttpGetNames = _ruleManager.GetAllHttpRuleDescriptions(HttpMethodType.Get);
            _rulesDescriptionVm.HttpPostNames = _ruleManager.GetAllHttpRuleDescriptions(HttpMethodType.Post);
            _rulesDescriptionVm.HttpPutNames = _ruleManager.GetAllHttpRuleDescriptions(HttpMethodType.Put);
            _rulesDescriptionVm.HttpDeleteNames = _ruleManager.GetAllHttpRuleDescriptions(HttpMethodType.Delete);
            _rulesDescriptionVm.HttpHeadNames = _ruleManager.GetAllHttpRuleDescriptions(HttpMethodType.Head);
            _rulesDescriptionVm.CustomNames = _ruleManager.GetAllCustomRuleDescriptions();
            return PartialView(_rulesDescriptionVm);
        }

        [HttpPost]
        public IActionResult SetRules([FromBody] List<string> rules)
        {
            var rulesToSave = rules.Where(p => p != null).Select(p => p);

            TempData["save"] = rulesToSave.ToArray();
            return Ok();
        }

        [HttpPost]
        public IActionResult ExportToPdf(string ruleCheckResultsInJson, string fileName)
        {
            var ruleCheckResults = JsonConvert.DeserializeObject<IEnumerable<RuleCheckResult>>(ruleCheckResultsInJson);
            var stream = _pdfCreator.Create(ruleCheckResults);
            return File(stream.ToArray(), System.Net.Mime.MediaTypeNames.Application.Pdf, $"{fileName.Split('.')[0]}.pdf");
        }

        [HttpPost]
        public IActionResult ExportToCsv(string ruleCheckResultsInJson, string fileName)
        {
            var ruleCheckResults = JsonConvert.DeserializeObject<IEnumerable<RuleCheckResult>>(ruleCheckResultsInJson);
            var csv = _csvCreator.Create(ruleCheckResults);

            return File(new UTF8Encoding().GetBytes(csv), "text/csv", $"{fileName.Split(".")[0]}.csv");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}