﻿using API_Validator.Services;
using API_Validator.Services.Interfaces;
using API_Validator.Services.Validators;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace API_Validator.DependencyInjection
{
    public static class IoCRegistrator
    {
        public static void Register(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddScoped<IPdfCreator, PdfCreator>();
            services.AddScoped<IConverter<IFormFile>, FileToStringConverter>();
            services.AddScoped<IValidationService, ValidationService>();
            services.AddScoped<IHttpMethodValidator, HttpMethodValidator>();
            services.AddScoped<ICustomRuleValidator, CustomRuleValidator>();
            services.AddScoped<IApiConsistencyValidator, ApiConsistencyValidator>();
            services.AddScoped<IApiConsistencyValidator, ApiConsistencyValidator>();
            services.AddScoped<IHttpMethodRuleFactory, HttpMethodRuleFactory>();
            services.AddScoped<ICustomRuleFactory, CustomRuleFactory>();
            services.AddScoped<IRuleManager, RuleManager>();
            services.AddScoped<IErrorSorter, ErrorSorter>();
            services.AddScoped<ICsvCreator, CsvCreator>();
        }
    }
}