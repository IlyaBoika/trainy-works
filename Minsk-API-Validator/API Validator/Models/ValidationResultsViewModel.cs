﻿using System.Collections.Generic;
using API_Validator.Services.Models;

namespace API_Validator.Models
{
    public class ValidationResultsViewModel
    {
        public IEnumerable<RuleCheckResult> ValidationResult { get; set; }
        public string Filename { get; set; }
    }
}