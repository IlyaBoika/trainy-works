﻿using System.Collections.Generic;
using API_Validator.Services.Models;

namespace API_Validator.Models
{
    public class RulesDescriptionViewModel
    {
        public IEnumerable<RuleDescription> HttpGetNames { get; set; }
        public IEnumerable<RuleDescription> HttpPostNames { get; set; }
        public IEnumerable<RuleDescription> HttpPutNames { get; set; }
        public IEnumerable<RuleDescription> HttpDeleteNames { get; set; }
        public IEnumerable<RuleDescription> HttpHeadNames { get; set; }
        public IEnumerable<RuleDescription> CustomNames { get; set; }
    }
}