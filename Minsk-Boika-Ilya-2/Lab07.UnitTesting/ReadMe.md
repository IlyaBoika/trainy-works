Lab 07. Unit Testing

 Создал проект Lab07.UnitTesting.Services.Tests(папка Geely.Services.Tests) для Unit-тестирования слоя сервисов,
Lab07.IntegrationTests - для интеграционных тестов.

Для написания тестов использовал NUnit, для создания fake/mock/stub - библиотеку moq, для анализа покрытия кода - dotCover.
