﻿using Geely.EntityFramework.Context;
using Geely.Repositories;
using NUnit.Framework;
using System.Linq;
using Geely.Domain;
using Microsoft.EntityFrameworkCore;

namespace Lab07.IntegrationTests
{
    public class AddCarToDatabaseIntegrationTest
    {
        private GeelyDbContext _dbContext;

        [SetUp]
        public void SetupContext()
        {
            _dbContext = new GeelyDbContext(BaseIntegrationTest.DbOptions);
        }

        [TearDown]
        public void CloseContext()
        {
            _dbContext.Database.CloseConnection();
        }

        [Test]
        public void AddCarToDatabaseAndGetCarsFromDatabaseCheck_HasExpectedresult()
        {
            // Arrange
            var _repository = new EFCarRepository(_dbContext);
            var originalCount = _repository.GetAllCars().Count();

            var car = new Car
            {
                Name = "TestCar",
                SearchField = "SearchField",
                Description = "Description",
                CarTypeId = 1
            };

            //Act
            _repository.Add(car);

            // Assert
            Assert.AreEqual(originalCount + 1, _repository.GetAllCars().Count());
        }
    }
}