﻿using Lab06.MVC;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Lab07.IntegrationTests
{
    [TestFixture]
    public class DefaultRequestShould
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public DefaultRequestShould()
        {
            // Arrange
            _server = new TestServer(new WebHostBuilder()
                //     .UseEnvironment("Test")
                .UseStartup<Startup>());
            _client = _server.CreateClient();
        }


        [TestCase("/")]
        [TestCase("/Home")]
        [TestCase("/Cars")]
        [TestCase("/Home/Privacy")]
        public async Task Get_EndpointsReturnSuccessAndCorrectContentType(string url)
        {
            // Act
            var response = await _client.GetAsync(url);

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
            Assert.AreEqual("text/html; charset=utf-8",
                response.Content.Headers.ContentType.ToString());
        }

        [Test]
        public async Task Get_SecurePageRequiresAnAuthenticatedUser()
        {
            // Act
            var response = await _client.GetAsync("/Cars/Order");

            // Assert
            Assert.AreEqual(HttpStatusCode.Redirect, response.StatusCode);
            Assert.AreEqual("/Account/Login", response.Headers.Location.OriginalString);
        }
    }
}