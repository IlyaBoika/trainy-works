﻿using Geely.EntityFramework.Context;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Lab07.IntegrationTests
{
    [SetUpFixture]
    public class BaseIntegrationTest
    {
        public static DbContextOptions<GeelyDbContext> DbOptions { get; private set; }
        private readonly string DatabaseName = "Geely_Test";

        [OneTimeSetUp]
        public void Setup()
        {
            DbOptions = new DbContextOptionsBuilder<GeelyDbContext>()
                .UseSqlServer($"Server= ;Database={DatabaseName};Trusted_Connection=True;")
                .Options;

            var _dbContext = new GeelyDbContext(DbOptions);
            _dbContext.Database.Migrate();
            _dbContext.Database.CloseConnection();
        }

        [OneTimeTearDown]
        public void Cleanup()
        {
            var _dbContext = new GeelyDbContext(DbOptions);
            _dbContext.Database.ExecuteSqlRaw($"USE master; DROP DATABASE [{DatabaseName}]");
            _dbContext.Database.CloseConnection();
        }
    }
}