﻿using Geely.Domain;
using Geely.EntityFramework.Mapping;
using Microsoft.EntityFrameworkCore;
using System;
using System.Security.Cryptography;

namespace Geely.EntityFramework.Context
{
    public class GeelyDbContext : DbContext
    {
        public GeelyDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Car> Cars { get; set; }

        public DbSet<CarUser> CarUsers { get; set; }

        public DbSet<CarType> CarTypes { get; set; }

        public DbSet<Role> Roles { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CarMap());

            modelBuilder.ApplyConfiguration(new UserMap());

            modelBuilder.ApplyConfiguration(new CarUsersMap());

            modelBuilder.ApplyConfiguration(new CarTypeMap());

            modelBuilder.ApplyConfiguration(new RoleMap());

            var adminPassword = GetPasswordHash("123456");


            var adminRole = new Role { Id = 1, Name = "admin" };
            var userRole = new Role { Id = 2, Name = "user" };
            var adminUser = new User { Id = 1, Username = "admin", Password = adminPassword, RoleId = adminRole.Id };

            var suv = new CarType { Id = 1, Type = "SUV" };
            var sedan = new CarType { Id = 2, Type = "sedan" };
            var hatchback = new CarType { Id = 3, Type = "hatchback" };
            var van = new CarType { Id = 4, Type = "van" };

            var emgrand = new Car { Id = 1, Name = "Emgrand", CarTypeId = 2, Description = "First sedan in stock", SearchField = "Emgrand First car in stock" };
            var atlas = new Car { Id = 2, Name = "Atlas", CarTypeId = 1, Description = "First SUV in stock", SearchField = "Atlas First SUV in stock" };

            modelBuilder.Entity<CarType>().HasData(new CarType[] { suv, sedan, hatchback, van });
            modelBuilder.Entity<Car>().HasData(new Car[] { emgrand, atlas });

            modelBuilder.Entity<Role>().HasData(new Role[] { adminRole, userRole });
            modelBuilder.Entity<User>().HasData(new User[] { adminUser });
            base.OnModelCreating(modelBuilder);
        }

        public static string GetPasswordHash(string password)
        {
            var salt = new byte[16];
            new RNGCryptoServiceProvider().GetBytes(salt);

            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            var hash = pbkdf2.GetBytes(20);

            var hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            var passwordHash = Convert.ToBase64String(hashBytes);

            return passwordHash;
        }
    }
}