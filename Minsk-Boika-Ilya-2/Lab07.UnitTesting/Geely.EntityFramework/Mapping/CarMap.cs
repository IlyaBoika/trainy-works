﻿using Geely.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Geely.EntityFramework.Mapping
{
    public class CarMap : IEntityTypeConfiguration<Car>
    {
        public void Configure(EntityTypeBuilder<Car> builder)
        {
            builder.ToTable("Cars");

            builder.HasKey(_ => _.Id);

            builder.Property(_ => _.Id).HasColumnName("Id").ValueGeneratedOnAdd();
            builder.Property(_ => _.Name).HasColumnName("Name").HasMaxLength(100).IsRequired();
            builder.Property(_ => _.SearchField).HasColumnName("SearchField").HasMaxLength(255).IsRequired();
            builder.Property(_ => _.Description).HasColumnName("Description").IsRequired();
            builder.Property(_ => _.CarTypeId).HasColumnName("CarTypeId").IsRequired();
        }
    }
}