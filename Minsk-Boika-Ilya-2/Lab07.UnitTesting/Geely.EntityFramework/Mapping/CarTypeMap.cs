﻿using Geely.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Geely.EntityFramework.Mapping
{
    public class CarTypeMap : IEntityTypeConfiguration<CarType>
    {
        public void Configure(EntityTypeBuilder<CarType> builder)
        {
            builder.ToTable("CarTypes");

            builder.HasKey(_ => _.Id);

            builder.Property(_ => _.Id).HasColumnName("Id").ValueGeneratedOnAdd();
            builder.Property(_ => _.Type).HasColumnName("Type").HasMaxLength(50).IsRequired();

            builder.HasMany(ct => ct.Cars)
                   .WithOne(c => c.CarType)
                   .HasForeignKey(c => c.CarTypeId);
        }
    }
}