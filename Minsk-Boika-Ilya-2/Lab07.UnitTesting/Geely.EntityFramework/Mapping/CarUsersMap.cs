﻿using Geely.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Geely.EntityFramework.Mapping
{
    public class CarUsersMap : IEntityTypeConfiguration<CarUser>
    {
        public void Configure(EntityTypeBuilder<CarUser> builder)
        {
            builder.ToTable("CarUsers");

            builder.Property(_ => _.CarId).HasColumnName("CarId").IsRequired();
            builder.Property(_ => _.UserId).HasColumnName("UserId").IsRequired();
            builder.Property(_ => _.TestdriveDate).HasColumnName("TestdriveDate").IsRequired();

            builder.HasKey(cu => new { cu.CarId, cu.UserId });

            builder.HasOne(cu => cu.User)
                   .WithMany(u => u.CarUsers)
                   .HasForeignKey(cu => cu.CarId);

            builder.HasOne(cu => cu.User)
                   .WithMany(c => c.CarUsers)
                   .HasForeignKey(cu => cu.UserId);
        }
    }
}