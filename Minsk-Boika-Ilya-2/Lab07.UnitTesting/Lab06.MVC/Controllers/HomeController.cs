﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Lab06.MVC.Models;
using Lab06.MVC.Models.Cars;
using Geely.Services.Interfaces;
using AutoMapper;
using Lab06.MVC.DTO;
using System.Collections.Generic;

namespace Lab06.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICarService _carService;
        private readonly IMapper _mapper;

        public HomeController(ICarService carService, IMapper mapper)
        {
            _carService = carService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            var cars = _carService.GetAllCars();

            var model = new CarsModel
            {
                Cars = _mapper.Map<List<CarDto>>(cars),
            };

            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}