﻿using AutoMapper;
using Geely.Domain;
using Lab06.MVC.DTO;

namespace Lab06.MVC.Mapping.Profiles
{
    public class CarDtoProfile:Profile
    {
       public CarDtoProfile()
        {
            CreateMap<Car, CarDto>();
            CreateMap<CarDto, Car>();
        }
    }
}