﻿using AutoMapper;
using Lab06.MVC.Mapping.Profiles;

namespace Lab06.MVC.Mapping
{
    public static class AutomapperConfigurator
    {
        public static IMapper Initialize()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(typeof(CarDtoProfile));
                cfg.AddProfile(typeof(CarTypeDtoProfile));
            });

            return new Mapper(config);
        }
    }
}