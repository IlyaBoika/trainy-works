﻿namespace Lab06.MVC.DTO
{
    public class CarTypeDto
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}