﻿using Geely.EntityFramework.Context;
using Geely.Repositories;
using Geely.Repositories.Interfaces;
using Geely.Services;
using Geely.Services.Interfaces;
using Lab06.MVC.Mapping;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Lab06.MVC.DependencyInjection
{
    public static class IoCRegistrator
    {
        public static void Register(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddDbContext<GeelyDbContext>(options => options.UseSqlServer(Constants.ConnectionString));

            services.AddScoped<ICarRepository, EFCarRepository>();
            services.AddScoped<ICarService, CarService>();
            services.AddScoped<IUserRepository, EFUserRepository>();
            services.AddScoped<IRecognitionService, RecognitionService>();
            services.AddScoped<IPasswordService, PasswordService>();
            services.AddScoped<ICarTypeRepository, EFCarTypeRepository>();
            services.AddScoped<ICarTypeService, CarTypeService>();
            services.AddScoped(_ => AutomapperConfigurator.Initialize());
        }
    }
}