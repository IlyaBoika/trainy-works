﻿using System.ComponentModel.DataAnnotations;

namespace Lab06.MVC.Models.Account
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Username not specified")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password not specified")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}