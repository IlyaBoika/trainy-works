﻿using Lab06.MVC.DTO;
using System.Collections.Generic;

namespace Lab06.MVC.Models.Cars
{
    public class CarsModel
    {
        public List<CarDto> Cars { get; set; }
        public string SearchText { get; set; }
    }
}
