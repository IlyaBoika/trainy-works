﻿using Lab06.MVC.DTO;
using System.Collections.Generic;

namespace Lab06.MVC.Models.Cars
{
    public class EditCarModel
    {
        public CarDto Car { get; set; }
        public List<CarTypeDto> CarTypes { get; set; }
    }
}