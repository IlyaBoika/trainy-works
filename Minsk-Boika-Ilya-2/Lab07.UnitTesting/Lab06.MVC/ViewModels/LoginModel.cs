﻿using System.ComponentModel.DataAnnotations;

namespace Lab06.MVC.ViewModels
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Не указан Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}