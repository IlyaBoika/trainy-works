﻿using Geely.Domain;
using System;
using System.Collections.Generic;

namespace Geely.Services.Interfaces
{
    public interface ICarService
    {
        void Add(Car car);
        List<Car> GetAllCars();
        Car GetById(int id);
        void Remove(int id);
        void Save(Car car);
        List<Car> Search(string searchText);
        void AddCarToUser(int carId, int userId, DateTime testdriveDate);
        bool IsCarOrderedByUser(int carId, int userId);
    }
}