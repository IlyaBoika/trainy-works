﻿using Geely.Domain;

namespace Geely.Services.Interfaces
{
    public interface IRecognitionService
    {
        User GetUserByUserNameAndPassword(string username, string password);
        void Add(User user);
        bool IsRegistred(string username);
    }
}