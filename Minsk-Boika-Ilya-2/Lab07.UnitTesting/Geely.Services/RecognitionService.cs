﻿using Geely.Domain;
using Geely.Repositories.Interfaces;
using Geely.Services.Interfaces;

namespace Geely.Services
{
    public class RecognitionService : IRecognitionService
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordService _passwordService;

        public RecognitionService(IUserRepository userRepository, IPasswordService passwordService)
        {
            _userRepository = userRepository;
            _passwordService = passwordService;
        }

        public User GetUserByUserNameAndPassword(string username, string password)
        {
            var retrievedUser = _userRepository.GetUserByUsername(username);

            if ((retrievedUser == null) || (!(_passwordService.IsRightPassword(retrievedUser.Password, password))))
            {
                return null;
            }

            return retrievedUser;
        }

        public void Add(User user)
        {
            var savedPasswordHash = _passwordService.GetPasswordHash(user.Password);

            user.Password = savedPasswordHash;

            _userRepository.Add(user);
        }

        public bool IsRegistred(string username)
        {
            var retrievedUser = _userRepository.GetUserByUsername(username);

            return retrievedUser != null;
        }
    }
}