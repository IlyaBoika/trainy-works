﻿using System.Collections.Generic;

namespace Geely.Services
{
    public static class Errors
    {
        private static Dictionary<string, string> ErrorsDictionary = new Dictionary<string, string>();

        static Errors()
        {
            ErrorsDictionary.Add(OrderErrors.CarAlreadyOrdered, "Sorry, you've earlier ordered this car for test-drive. Please, choose another car.");
        }

        public static string GetMessage(string key)
        {
            return ErrorsDictionary[key];
        }
    }
}