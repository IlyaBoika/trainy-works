﻿using Geely.Services.Interfaces;
using System.Collections.Generic;
using Geely.Repositories.Interfaces;
using Geely.Domain;

namespace Geely.Services
{
    public class CarTypeService : ICarTypeService
    {
        private readonly ICarTypeRepository _carTypeRepository;

        public CarTypeService(ICarTypeRepository carTypeRepository)
        {
            _carTypeRepository = carTypeRepository;
        }

        public List<CarType> GetAllCarTypes()
        {
            return _carTypeRepository.GetAllCarTypes();
        }
    }
}