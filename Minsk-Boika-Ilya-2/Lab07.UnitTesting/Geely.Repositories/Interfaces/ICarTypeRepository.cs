﻿using Geely.Domain;
using System.Collections.Generic;

namespace Geely.Repositories.Interfaces
{
    public interface ICarTypeRepository
    {
        List<CarType> GetAllCarTypes();
    }
}