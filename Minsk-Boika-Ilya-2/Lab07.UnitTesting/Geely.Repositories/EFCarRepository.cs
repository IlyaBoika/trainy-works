﻿using Geely.Domain;
using Geely.EntityFramework.Context;
using Geely.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Geely.Repositories
{
    public class EFCarRepository : ICarRepository
    {
        private readonly GeelyDbContext _context;

        public EFCarRepository(GeelyDbContext context)
        {
            _context = context;
        }

        public void Add(Car car)
        {
            _context.Add(car);
            _context.SaveChanges();
        }

        public List<Car> GetAllCars()
        {
            return _context.Cars.Include(_ => _.CarType).ToList();
        }

        public Car GetById(int id)
        {
            return _context
                   .Cars
                   .Include(_ => _.CarType)
                   .First(_ => _.Id == id);
        }

        public void Remove(int id)
        {
            var car = _context.Cars.First(_ => _.Id == id);

            _context.Cars.Remove(car);
            _context.SaveChanges();
        }

        public void Save(Car car)
        {
            if (car.Id == 0)
            {
                _context.Add(car);
            }
            else
            {
                _context.Attach(car);
                _context.Entry(car).State = EntityState.Modified;
            }

            _context.SaveChanges();
        }

        public List<Car> Search(string searchText)
        {
            return _context.Cars.Include(_ => _.CarType).Where(_ => _.SearchField.Contains(searchText)).ToList();
        }

        public void AddCarToUser(int carId, int userId, DateTime testdriveDate)
        {
            var carUser = new CarUser()
            {
                CarId = carId,
                UserId = userId,
                TestdriveDate = testdriveDate,
            };
            _context.Add(carUser);
            _context.SaveChanges();
        }

        public bool IsCarOrderedByUser(int carId, int userId)
        {
            var carUser = _context.CarUsers.FirstOrDefault(_ => _.CarId == carId && _.UserId == userId);

            return carUser != null;
        }
    }
}