﻿using System.Collections.Generic;

namespace Geely.Domain
{
    public class Car
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SearchField { get; set; }
        public string Description { get; set; }
        public int CarTypeId { get; set; }

        #region Navigation Properties

        public List<CarUser> CarUsers { get; set; }
        public CarType CarType { get; set; }

        #endregion
    }
}