﻿using Geely.Services;
using NUnit.Framework;

namespace Lab07.UnitTesting.Services.Tests
{
    [TestFixture]
    public class PasswordServiceTest
    {
        #region GetPasswordHash

        [TestCase("12345")]
        [TestCase("32_QAZxc1321")]
        public void GetPasswordHash_General(string password)

        {
            // Arrange
            var service = new PasswordService();

            //Act
            var hash = service.GetPasswordHash(password);

            //Assert
            Assert.IsTrue(service.IsRightPassword(hash, password));
        }

        [TestCase("12345", "123456")]
        [TestCase("32_QAZxc1321", "qwerty")]
        public void IsRightPassword_FalsePassword_False(string password, string wrongPassword)

        {
            // Arrange
            var service = new PasswordService();

            //Act
            var hash = service.GetPasswordHash(password);

            //Assert
            Assert.IsFalse(service.IsRightPassword(hash, wrongPassword));
        }

        #endregion
    }
}