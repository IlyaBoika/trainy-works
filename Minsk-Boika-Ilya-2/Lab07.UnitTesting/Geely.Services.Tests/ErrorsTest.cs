﻿using Geely.Services;
using NUnit.Framework;

namespace Lab07.UnitTesting.Services.Tests
{
    [TestFixture]
    public class ErrorsTest
    {
        [Test]
        public void GetMessage_General()
        {
            //Arrange
            const string expectedresult = "Sorry, you've earlier ordered this car for test-drive. Please, choose another car.";
            const string key = "Order.CarAlreadyOrdered";

            //Act
            var result = Errors.GetMessage(key);

            //Assert
            Assert.AreEqual(result, expectedresult);
        }
    }
}