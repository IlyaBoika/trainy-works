﻿using System;
using System.Collections.Generic;
using System.Linq;
using Geely.Domain;
using Geely.Repositories.Interfaces;
using Geely.Services;
using Moq;
using NUnit.Framework;

namespace Lab07.UnitTesting.Services.Tests
{
    [TestFixture]
    public class CarServiceTest
    {
        private static readonly int Id = 1;
        private static readonly string Description = "Description";
        private static readonly int CarTypeId = 1;
        private static readonly string Name = "Name";
        private static readonly int CarId = 1;
        private static readonly int UserId = 2;
        private static readonly DateTime TestdriveDate = new DateTime(2020, 4, 7);

        private Mock<ICarRepository> carRepositoryMock;
        private CarService service;

        [SetUp]
        public void BeforeTest()
        {
             carRepositoryMock = new Mock<ICarRepository>();
             service = new CarService(carRepositoryMock.Object);
        }


        #region Add

        [Test]
        public void Add_HasExpectedResult()
        {
            //Arrange
            var car = new Car
            {
                Id = Id,
                Name = Name,
                Description = Description,
                CarTypeId = CarTypeId,
            };

            var searchField = GetSearchField();

            //Act
            service.Add(car);

            //Assert
            carRepositoryMock.Verify(_ => _.Add(It.Is<Car>(c => c.Id == Id
                                                                  && c.Name == Name
                                                                  && c.Description == Description
                                                                  && c.CarTypeId == CarTypeId
                                                                  && c.SearchField == searchField)), Times.Once);
        }

        #endregion

        #region GetAllCars

        [Test]
        public void GetAllCars_HasExpectedResult()
        {
            //Arrange
            var searchField = GetSearchField();

            var expectedCars = new List<Car>
            {
                new Car
                {
                Id = Id,
                Name = Name,
                Description = Description,
                CarTypeId = CarTypeId,
                SearchField = searchField,
                },

                new Car
                {
                Id = 2,
                }
            };

            carRepositoryMock.Setup(_ => _.GetAllCars()).Returns(expectedCars);

            //Act
            var actualcars = service.GetAllCars();

            //Assert
            carRepositoryMock.Verify(_ => _.GetAllCars(), Times.Once);

            var firstCar = actualcars.First();

            Assert.AreEqual(Id, firstCar.Id);
            Assert.AreEqual(Name, firstCar.Name);
            Assert.AreEqual(Description, firstCar.Description);
            Assert.AreEqual(CarTypeId, firstCar.CarTypeId);
            Assert.AreEqual(searchField, firstCar.SearchField);

            Assert.IsNotNull(actualcars.ElementAt(1));
        }

        #endregion

        #region GetById

        [Test]
        public void GetById_HasExpectedResult()
        {
            //Arrange
            var searchField = GetSearchField();

            var expectedCar = new Car
            {
                Id = Id,
                Name = Name,
                Description = Description,
                CarTypeId = CarTypeId,
                SearchField = searchField,
            };

            carRepositoryMock.Setup(_ => _.GetById(Id)).Returns(expectedCar);

            //Act
            var actualCar = service.GetById(Id);

            //Assert
            carRepositoryMock.Verify(_ => _.GetById(It.Is<int>(i => i == Id)), Times.Once);

            Assert.AreEqual(Id, actualCar.Id);
            Assert.AreEqual(Name, actualCar.Name);
            Assert.AreEqual(Description, actualCar.Description);
            Assert.AreEqual(CarTypeId, actualCar.CarTypeId);
            Assert.AreEqual(searchField, actualCar.SearchField);
        }

        #endregion

        #region Remove

        [Test]
        public void Remove_General()
        {
            //Act
            service.Remove(Id);

            //Assert
            carRepositoryMock.Verify(_ => _.Remove(It.Is<int>(i => i == Id)), Times.Once);
        }

        #endregion

        #region Save

        [Test]
        public void Save_HasExpectedResult()
        {
            //Arrange
            var searchField = GetSearchField();

            var car = new Car
            {
                Id = Id,
                Name = Name,
                Description = Description,
                CarTypeId = CarTypeId,
            };

            //Act
            service.Save(car);

            //Assert
            carRepositoryMock.Verify(_ => _.Save(It.Is<Car>(c => c.Id == Id
                                                              && c.Name == Name
                                                              && c.Description == Description
                                                              && c.CarTypeId == CarTypeId
                                                              && c.SearchField == searchField)), Times.Once);
        }

        #endregion

        #region Search

        [Test]
        public void Search_HasExpectedResult()
        {
            //Arrange
            var searchText = "SearchText";

            var searchField = GetSearchField();

            var expectedPhones = new List<Car>
            {
                new Car
                {
                    Id = Id,
                    Name = Name,
                    Description = Description,
                    CarTypeId = CarTypeId,
                    SearchField = searchField,
                },
                new Car
                {
                    Id = 2,
                }
            };


            carRepositoryMock.Setup(_ => _.Search(searchText)).Returns(expectedPhones);

            //Act
            var actualcars = service.Search(searchText);

            //Assert
            carRepositoryMock.Verify(_ => _.Search(It.Is<string>(s => s == searchText)), Times.Once);

            var firstCar = actualcars.First();

            Assert.AreEqual(Id, firstCar.Id);
            Assert.AreEqual(Name, firstCar.Name);
            Assert.AreEqual(Description, firstCar.Description);
            Assert.AreEqual(CarTypeId, firstCar.CarTypeId);
            Assert.AreEqual(searchField, firstCar.SearchField);

            Assert.IsNotNull(actualcars.ElementAt(1));
        }

        #endregion

        #region IsCarOrderedByUser

        [Test]
        public void IsCarOrderedByUser_HasExpectedResult()
        {
            //Act
            service.IsCarOrderedByUser(CarId, UserId);

            //Assert
            carRepositoryMock.Verify(_ => _.IsCarOrderedByUser(It.Is<int>(i => i == CarId), It.Is<int>(i => i == UserId)), Times.Once);
        }

        #endregion

        #region IsCarOrderedByUser

        [Test]
        public void AddCarToUser_HasExpectedResult()
        {
            //Act
            service.AddCarToUser(CarId, UserId, TestdriveDate);

            //Assert
            carRepositoryMock.Verify(_ => _.AddCarToUser(It.Is<int>(i => i == CarId), It.Is<int>(i => i == UserId), It.Is<DateTime>(i => i == TestdriveDate)), Times.Once);
        }

        #endregion


        #region Helpers

        private static string GetSearchField()
        {
            return (Name + " " + Description).ToUpper();
        }

        #endregion
    }
}