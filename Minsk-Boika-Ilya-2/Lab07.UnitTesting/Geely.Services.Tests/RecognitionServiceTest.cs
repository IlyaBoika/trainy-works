﻿using System;
using Geely.Domain;
using Geely.Repositories.Interfaces;
using Geely.Services;
using Geely.Services.Interfaces;
using Moq;
using NUnit.Framework;

namespace Lab07.UnitTesting.Services.Tests
{
    [TestFixture]
    class RecognitionServiceTest
    {
        private static readonly string PasswordHash = "savedPasswordHash";
        private static readonly int Id = 1;
        private static readonly string Username = "Name";
        private static readonly string NotRegistredUsername = "NotRegistredUsername";
        private static readonly string Password = "Password";
        private static readonly string FalsePassword = "FalsePassword";
        private static readonly DateTime CreationDate = new DateTime(2020, 4, 7);
        private static readonly int RoleId = 1;

        private Mock<IUserRepository> userRepositoryMock;
        private RecognitionService service;
        private Mock<IPasswordService> passwordServiceMock;

        [SetUp]
        public void BeforeTest()
        {
             userRepositoryMock = new Mock<IUserRepository>();
             passwordServiceMock = new Mock<IPasswordService>();
             service = new RecognitionService(userRepositoryMock.Object, passwordServiceMock.Object);
        }

        #region Add

        [Test]
        public void Add_HasExpectedResult()
        {
            //Arrange
            var user = CreateUser();

            passwordServiceMock.Setup(_ => _.GetPasswordHash(user.Password)).Returns(PasswordHash);

            //Act
            service.Add(user);

            //Assert
            userRepositoryMock.Verify(_ => _.Add(It.Is<User>(u => u.Id == Id
                                                                && u.Username == Username
                                                                && u.Password == PasswordHash
                                                                && u.CreationDate == CreationDate
                                                                && u.RoleId == RoleId)), Times.Once);
        }

        #endregion

        #region IsRegistred

        [Test]
        public void IsRegistred_UserIsRegistred_ReturnsTrue()
        {
            //Arrange
            var user = CreateUser();

            userRepositoryMock.Setup(_ => _.GetUserByUsername(Username)).Returns(user);

            //Act
            var result = service.IsRegistred(Username);

            //Assert
            userRepositoryMock.Verify(_ => _.GetUserByUsername(It.Is<string>(i => i == Username)), Times.Once);
            Assert.AreEqual(result, true);
        }

        [Test]
        public void IsRegistered_UserIsNotRegistered_ReturnsFalse()
        {
            //Arrange
            userRepositoryMock.Setup(_ => _.GetUserByUsername(NotRegistredUsername)).Returns<User>(null);

            //Act
            var result = service.IsRegistred(NotRegistredUsername);

            //Assert
            userRepositoryMock.Verify(_ => _.GetUserByUsername(It.Is<string>(i => i == NotRegistredUsername)), Times.Once);
            Assert.AreEqual(result, false);
        }

        #endregion


        #region GetUserByUserNameAndPassword

        [Test]
        public void GetUserByUserNameAndPassword_UserIsRegistredAndInputRightPassword_ReturnsUser()
        {
            //Arrange
            var user = CreateUser();

            userRepositoryMock.Setup(_ => _.GetUserByUsername(Username)).Returns(user);

            passwordServiceMock.Setup(_ => _.IsRightPassword(user.Password, Password)).Returns(true);

            //Act
            var result = service.GetUserByUserNameAndPassword(Username, Password);

            //Assert
            userRepositoryMock.Verify(_ => _.GetUserByUsername(It.Is<string>(i => i == Username)), Times.Once);
            Assert.AreEqual(result, user);
        }

        [Test]
        public void GetUserByUserNameAndPassword_UserIsRegistredAndInputFalsePassword_ReturnsNull()
        {
            //Arrange
            var user = CreateUser();

            userRepositoryMock.Setup(_ => _.GetUserByUsername(Username)).Returns(user);

            passwordServiceMock.Setup(_ => _.IsRightPassword(user.Password, FalsePassword)).Returns(false);

            //Act
            var result = service.GetUserByUserNameAndPassword(Username, FalsePassword);

            //Assert
            userRepositoryMock.Verify(_ => _.GetUserByUsername(It.Is<string>(i => i == Username)), Times.Once);
            Assert.AreEqual(result, null);
        }

        [Test]
        public void GetUserByUserNameAndPassword_UserIsNotRegistredAndInputFalsePassword_ReturnsNull()
        {
            //Arrange
            var user = CreateUser();

            userRepositoryMock.Setup(_ => _.GetUserByUsername(Username)).Returns<User>(null);

            passwordServiceMock.Setup(_ => _.IsRightPassword(user.Password, FalsePassword)).Returns(false);

            //Act
            var result = service.GetUserByUserNameAndPassword(Username, FalsePassword);

            //Assert
            userRepositoryMock.Verify(_ => _.GetUserByUsername(It.Is<string>(i => i == Username)), Times.Once);
            Assert.AreEqual(result, null);
        }

        [Test]
        public void GetUserByUserNameAndPassword_UserIsNotRegistredAndInputRightPassword_ReturnsNull()
        {
            //Arrange
            var user = CreateUser();

            userRepositoryMock.Setup(_ => _.GetUserByUsername(Username)).Returns<User>(null);

            passwordServiceMock.Setup(_ => _.IsRightPassword(user.Password, FalsePassword)).Returns(true);

            //Act
            var result = service.GetUserByUserNameAndPassword(Username, FalsePassword);

            //Assert
            userRepositoryMock.Verify(_ => _.GetUserByUsername(It.Is<string>(i => i == Username)), Times.Once);
            Assert.AreEqual(result, null);
        }

        #endregion

        private User CreateUser()
        {
            return new User
            {
                Id = Id,
                Username = Username,
                Password = PasswordHash,
                CreationDate = CreationDate,
                RoleId = RoleId,
            };
        }
    }
}