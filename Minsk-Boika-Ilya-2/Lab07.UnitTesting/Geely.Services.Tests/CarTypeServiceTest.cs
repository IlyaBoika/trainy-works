﻿using System.Collections.Generic;
using System.Linq;
using Geely.Domain;
using Geely.Repositories.Interfaces;
using Geely.Services;
using Moq;
using NUnit.Framework;

namespace Lab07.UnitTesting.Services.Tests
{
    [TestFixture]
    public class CarTypeServiceTest
    {
        private static readonly int Id = 1;
        private static readonly string Type = "Sedan";

        #region GetAllCars

        [Test]
        public void GetAllCarTypes_HasExpectedResult()
        {
            //Arrange
            var carTypeRepositoryMock = new Mock<ICarTypeRepository>();
            var service = new CarTypeService(carTypeRepositoryMock.Object);

            var expectedCarTypes = new List<CarType>
            {
                new CarType
                {
                    Id = Id,
                    Type = Type,
                },

                new CarType
                {
                    Id = 2,
                }
            };

            carTypeRepositoryMock.Setup(_ => _.GetAllCarTypes()).Returns(expectedCarTypes);

            //Act
            var actualCarTypes = service.GetAllCarTypes();

            //Assert
            carTypeRepositoryMock.Verify(_ => _.GetAllCarTypes(), Times.Once);

            var firstCarType = actualCarTypes.First();

            Assert.AreEqual(Id, firstCarType.Id);
            Assert.AreEqual(Type, firstCarType.Type);


            Assert.IsNotNull(actualCarTypes.ElementAt(1));
        }

        #endregion
    }
}
