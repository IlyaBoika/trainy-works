﻿using Geely.EntityFramework.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Geely.EntityFramework.Migrations
{
    public class DbContextFactory : IDesignTimeDbContextFactory<GeelyDbContext>
    {
        public GeelyDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<GeelyDbContext>();

            builder.UseSqlServer(@"Server= ;Database=Geely;Trusted_Connection=True;");

            return new GeelyDbContext(builder.Options);
        }
    }
}