﻿namespace Geely.Services
{
    public static class OrderErrors
    {
        public static readonly string CarAlreadyOrdered = "Order.CarAlreadyOrdered";
    }
}