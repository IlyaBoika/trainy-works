﻿using Geely.Domain;
using System.Collections.Generic;

namespace Geely.Services.Interfaces
{
    public interface ICarTypeService
    {
        List<CarType> GetAllCarTypes();
    }
}