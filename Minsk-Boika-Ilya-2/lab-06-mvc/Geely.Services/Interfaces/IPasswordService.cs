﻿namespace Geely.Services.Interfaces
{
    public interface IPasswordService
    {
        string GetPasswordHash(string password);
        bool IsRightPassword(string passwordHash, string password);
    }
}