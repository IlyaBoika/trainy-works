﻿using Geely.Domain;
using Geely.Repositories.Interfaces;
using Geely.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace Geely.Services
{
    public class CarService : ICarService
    {
        private readonly ICarRepository _carRepository;

        public CarService(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public void Add(Car car)
        {
            car.SearchField = (car.Name + car.Description).ToUpper();
            _carRepository.Add(car);
        }


        public void AddCarToUser(int carId, int userId, DateTime testdriveDate)
        {
            _carRepository.AddCarToUser(carId, userId, testdriveDate);
        }

        public List<Car> GetAllCars()
        {
            return _carRepository.GetAllCars();
        }

        public Car GetById(int id)
        {
            return _carRepository.GetById(id);
        }

        public bool IsCarOrderedByUser(int carId, int userId)
        {
            return _carRepository.IsCarOrderedByUser(carId, userId);
        }

        public void Remove(int id)
        {
            _carRepository.Remove(id);
        }

        public void Save(Car car)
        {
            car.SearchField = (car.Name + car.Description).ToUpper();
            _carRepository.Save(car);
        }

        public List<Car> Search(string searchText)
        {
            return _carRepository.Search(searchText);
        }
    }
}