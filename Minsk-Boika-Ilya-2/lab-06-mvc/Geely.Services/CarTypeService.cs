﻿using Geely.Domain;
using Geely.Repositories.Interfaces;
using Geely.Services.Interfaces;
using System.Collections.Generic;

namespace Geely.Services
{
    public class CarTypeService : ICarTypeService
    {
        private readonly ICarTypeRepository _carTypeRepository;

        public CarTypeService(ICarTypeRepository carTypeRepository)
        {
            _carTypeRepository = carTypeRepository;
        }

        public List<CarType> GetAllCarTypes()
        {
            return _carTypeRepository.GetAllCarTypes();
        }
    }
}