﻿using Geely.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Geely.EntityFramework.Mapping
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder.HasKey(_ => _.Id);

            builder.Property(_ => _.Id).HasColumnName("Id").ValueGeneratedOnAdd();
            builder.Property(_ => _.Username).HasColumnName("Username").HasMaxLength(100).IsRequired();
            builder.Property(_ => _.Password).HasColumnName("Password").HasMaxLength(100).IsRequired();
            builder.Property(_ => _.CreationDate).HasColumnName("CreationDate").IsRequired();
            builder.Property(_ => _.RoleId).HasColumnName("RoleId").IsRequired();
        }
    }
}