﻿using AutoMapper;
using Geely.Domain;
using Lab06.MVC.DTO;

namespace Lab06.MVC.Mapping.Profiles
{
    public class CarTypeDtoProfile:Profile
    {
        public CarTypeDtoProfile()
        {
            CreateMap<CarType, CarTypeDto>();
        }
    }
}
