﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Geely.Domain;
using Geely.Services;
using Geely.Services.Interfaces;
using Lab06.MVC.DTO;
using Lab06.MVC.Models.Cars;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.MVC.Controllers
{
    public class CarsController : Controller
    {
        private readonly ICarService _carService;

        private readonly ICarTypeService _carTypeService;

        private readonly IMapper _mapper;

        public CarsController(ICarService carService, ICarTypeService carTypeService, IMapper mapper)
        {
            _carService = carService;
            _carTypeService = carTypeService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index(string searchText)
        {
            List<Car> cars;

            if (string.IsNullOrWhiteSpace(searchText))
            {
                cars = _carService.GetAllCars();
            }
            else
            {
                cars = _carService.Search(searchText);
            }

            var model = new CarsModel()
            {
                Cars = _mapper.Map<List<CarDto>>(cars),
                SearchText = searchText
            };

            return View(model);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult Edit(int id)
        {
            ViewData["ActionType"] = "Edit";

            var car = _carService.GetById(id);
            var carTypes = _carTypeService.GetAllCarTypes();

            var model = new EditCarModel
            {
                Car = _mapper.Map<CarDto>(car),
                CarTypes = _mapper.Map<List<CarTypeDto>>(carTypes)
            };

            return View(model);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult Edit(EditCarModel model)
        {
            if (ModelState.IsValid)
            {
                var car = _mapper.Map<Car>(model.Car);
                _carService.Save(car);
                return RedirectToAction("Index");
            }

            var carTypes = _carTypeService.GetAllCarTypes();
            model.CarTypes = _mapper.Map<List<CarTypeDto>>(carTypes);

            return View(model);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult Create()
        {
            ViewData["ActionType"] = "Create";

            var carTypes = _carTypeService.GetAllCarTypes();

            var model = new EditCarModel
            {
                Car = new CarDto(),
                CarTypes = _mapper.Map<List<CarTypeDto>>(carTypes),
            };

            return View("Edit", model);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult Remove(int id)
        {
            _carService.Remove(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Order(int carId)
        {
            if (User.IsInRole("user"))
            {
                var userId = int.Parse(Request.HttpContext.User.Claims.SingleOrDefault(c => c.Type == "Id")?.Value);

                if (_carService.IsCarOrderedByUser(carId, userId))
                {
                    return RedirectToAction("Error", new { error = OrderErrors.CarAlreadyOrdered });
                }

                ViewData["ActionType"] = "Order";

                var model = new AddCarToUserModel
                {
                    CarId = carId,
                    UserId = userId,
                };

                return View(model);
            }

            TempData["shortMessage"] = "You need to log in or register before test-drive ordering";

            return RedirectToAction("Login", "Account");
        }

        [Authorize(Roles = "user")]
        [HttpPost]
        public IActionResult Order(AddCarToUserModel model)
        {
            _carService.AddCarToUser(model.CarId, model.UserId, model.TestdriveDate);
            return RedirectToAction("Index");
        }

        public IActionResult Error(string error)
        {
            ViewBag.ErrorMessage = Errors.GetMessage(error);

            return View();
        }
    }
}