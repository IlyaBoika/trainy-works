﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Geely.Domain;
using Geely.Services.Interfaces;
using Lab06.MVC.Models.Account;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.MVC.Controllers
{
    public class AccountController : Controller
    {
        private readonly IRecognitionService _recognitionService;

        public AccountController(IRecognitionService recognitionService)
        {
            _recognitionService = recognitionService;
        }

        [HttpGet]
        public IActionResult Login()
        {
            ViewBag.Message = TempData["shortMessage"]?.ToString();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _recognitionService.GetUserByUserNameAndPassword(model.Username, model.Password);

                if (user != null)
                {
                    await Authenticate(user);

                    return RedirectToAction("Index", "Cars");
                }

                ModelState.AddModelError("", "Incorrect username and / or password");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Register()
        {           
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var isRegistred = _recognitionService.IsRegistred(model.Username);

                if (!isRegistred)
                {
                    var user = new  User { Username = model.Username, Password = model.Password, Role = new Role() { Name = "user" }, RoleId = 2 };

                    _recognitionService.Add(user);

                    await Authenticate(user); 

                    return RedirectToAction("Index", "Cars");
                }

                ModelState.AddModelError("", "User with the same name already exists");
            }

            return View(model);
        }

        private async Task Authenticate(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Username),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role?.Name),
                new Claim("Id", user.Id.ToString()),
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Cars");
        }
    }
}