﻿using System.ComponentModel.DataAnnotations;

namespace Lab06.MVC.DTO
{
    public class CarDto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "please, add name of car")]
        public string Name { get; set; }

        public string SearchField { get; set; }


        [Required(ErrorMessage = "please, add description  of car")]
        public string Description { get; set; }


        [Required(ErrorMessage = "please, choose bodytype of car")]
        public int? CarTypeId { get; set; }

        public CarTypeDto CarType { get; set; }
    }
}