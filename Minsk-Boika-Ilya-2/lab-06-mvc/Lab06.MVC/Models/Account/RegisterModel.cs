﻿using System.ComponentModel.DataAnnotations;

namespace Lab06.MVC.Models.Account
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Username not specified")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password not specified")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "password must have at least 3 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Password entered incorrectly")]
        public string ConfirmPassword { get; set; }
    }
}