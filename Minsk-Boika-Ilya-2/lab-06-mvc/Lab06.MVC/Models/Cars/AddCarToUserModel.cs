﻿using System;

namespace Lab06.MVC.Models.Cars
{
    public class AddCarToUserModel
    {
        public int CarId { get; set; }
        public int UserId { get; set; }
        public DateTime TestdriveDate { get; set; }
    }
}