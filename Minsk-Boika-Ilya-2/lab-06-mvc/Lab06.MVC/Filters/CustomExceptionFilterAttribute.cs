﻿using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Reflection;

namespace FiltersApp.Filters
{
    public class CustomExceptionFilterAttribute : Attribute, IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            string actionName = context.ActionDescriptor.DisplayName;
            string exceptionStack = context.Exception.StackTrace;
            string exceptionMessage = context.Exception.Message;
            context.Result = new ContentResult
            {
                Content = $"An exception occurred in the {actionName} method: \n {exceptionMessage} \n {exceptionStack}"
            };
            LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType).Error($"An exception occurred in the {actionName} method: \n {exceptionMessage} \n {exceptionStack}");
            context.ExceptionHandled = true;
        }
    }
}