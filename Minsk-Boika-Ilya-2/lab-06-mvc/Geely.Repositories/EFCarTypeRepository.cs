﻿using Geely.Domain;
using Geely.EntityFramework.Context;
using Geely.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Geely.Repositories
{
    public class EFCarTypeRepository : ICarTypeRepository
    {
        private readonly GeelyDbContext _context;

        public EFCarTypeRepository(GeelyDbContext context)
        {
            _context = context;
        }

        public List<CarType> GetAllCarTypes()
        {
            return _context.CarTypes.ToList();
        }
    }
}