﻿using Geely.Domain;
using Geely.EntityFramework.Context;
using Geely.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Geely.Repositories
{
    public class EFUserRepository:IUserRepository
    {
        private readonly GeelyDbContext _context;

        public EFUserRepository(GeelyDbContext context)
        {
            _context = context;
        }

        public void Add(User user)
        {
            _context.Add(user);
            _context.SaveChanges();
        }

        public User GetUserByUsername(string username)
        {
            return _context.Users
                .Include(u => u.Role)
                .SingleOrDefault(_ => _.Username == username);
        }
    }
}