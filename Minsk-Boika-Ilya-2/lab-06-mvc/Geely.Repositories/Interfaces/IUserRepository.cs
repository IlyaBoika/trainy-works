﻿using Geely.Domain;

namespace Geely.Repositories.Interfaces
{
    public interface IUserRepository
    {
        void Add(User user);
        User GetUserByUsername(string username);
    }
}