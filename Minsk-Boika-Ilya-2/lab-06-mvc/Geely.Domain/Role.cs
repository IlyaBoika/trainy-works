﻿using System.Collections.Generic;

namespace Geely.Domain
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }

        #region Naviagation Properties

        public List<User> Users { get; set; }

        public Role()
        {
            Users = new List<User>();
        }

        #endregion
    }
}