﻿using System;
using System.Collections.Generic;

namespace Geely.Domain
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime CreationDate { get; set; }

        public int? RoleId { get; set; }

        #region Navigation Properties

        public List<CarUser> CarUsers { get; set; }

        public Role Role { get; set; }

        #endregion
    }
}