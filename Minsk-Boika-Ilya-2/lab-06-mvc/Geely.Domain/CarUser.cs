﻿using System;

namespace Geely.Domain
{
    public class CarUser
    {
        public int CarId { get; set; }
        public int UserId { get; set; }

        public DateTime TestdriveDate { get; set; }


        #region Navigation Properties

        public User User { get; set; }
        public Car Car { get; set; }

        #endregion
    }
}