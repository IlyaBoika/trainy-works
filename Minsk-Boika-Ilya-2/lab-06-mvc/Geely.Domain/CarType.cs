﻿using System.Collections.Generic;

namespace Geely.Domain
{
    public class CarType
    {
        public int Id { get; set; }
        public string Type { get; set; }

        #region Navigation Properties

        public List<Car> Cars { get; set; }

        #endregion
    }
}