﻿using Export;
using FileCommander.Repositories;
using FileCommander.Repositories.Interfaces;
using FileCommander.Toolbox.Logger;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using FileCommander.Services;
using FileCommander.Services.Interfaces;
using FileCommander.UI.Console;
using FileCommander.UI.Console.Interfaces;
using FileCommander.EntityFramework.Context;
using Microsoft.EntityFrameworkCore;
using FileCommander.Repositories.EF;

namespace FileCommander
{
    static class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            var connectionString = ConfigurationManager.AppSettings.Get("ConnectionString");

            var exporters = LoadExporters();

            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddDbContext<FileCommanderDbContext>(options => options.UseSqlServer(connectionString))
                .AddSingleton<ICommandProcessor, CommandProcessor>()
                // .AddSingleton(typeof(IUserRepository), (provider) => new SecretStorageUserRepository(username, password))
                //.AddSingleton<IUserRepository, InMemoryUserRepository>()
                .AddSingleton<IUserRepository, EFUserRepository>()
                .AddSingleton<IPasswordService, PasswordService>()
                .AddSingleton<IAuthenticationService, AuthenticationService>()
                .AddSingleton<IFileService, FileService>()
                .AddSingleton<ISerializer, BinarySerializer>()
                .AddSingleton<InMemoryDatabase, InMemoryDatabase>()
                .AddSingleton<IDeserilizer, BinaryDeserializer>()
                .AddSingleton<IFileComparing, HashComparing>()
                .AddSingleton<IMetadataExportService, MetadataStandardExportService>()
                .AddSingleton(typeof(List<IExporter>), (provider) => exporters)
                .AddSingleton<IMetadataService, MetadaService>()
                //     .AddSingleton<IFileRepository, BinaryFileMetadataRepository>()
                .AddSingleton<IFileRepository, EFMetadataRepository>()
                .BuildServiceProvider();

            var commandProcessor = serviceProvider.GetService<ICommandProcessor>();
            commandProcessor.Run();
        }

        private static List<IExporter> LoadExporters()
        {
            var exporters = new List<IExporter>();

            var dllPath = ConfigurationManager.AppSettings.Get("dllPath");
            var dlls = Directory.GetFiles(dllPath, "*.dll");

            foreach (var dll in dlls)
            {
                try
                {
                    var assembly = Assembly.LoadFrom(dll);

                    var plugins = assembly.GetTypes().Where(type => typeof(IExporter).IsAssignableFrom(type));

                    foreach (var plugin in plugins)
                    {
                        var instance = (IExporter) Activator.CreateInstance(plugin);
                        exporters.Add(instance);
                    }
                }
                catch
                {
                    Console.WriteLine("error loading plugin");
                }
            }

            return exporters;
        }

        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e.ExceptionObject.ToString());
            Console.WriteLine("Press Enter to continue");
            Console.ReadLine();
            Logger.Log((e.ExceptionObject as Exception)?.Message, LogType.Fatal);
            Logger.Log((e.ExceptionObject as Exception)?.StackTrace, LogType.Fatal);
        }
    }
}