 Приложение поддерживает одного пользователя. login - ilya, пароль - pass. Файл с данными для входа в приложение хранится в Secret storage локального компьютера.
Для проверки работы приложения на удаленном компьютере создал класс InMemoryUserRepository, логин и пароль в котором хранятся в памяти приложения.
Для этого нужно в классе Program изменить регистрацию DI c InMemoryUserRepository на SecretStorageUserRepository. При запуске приложения нужно ввести команду login ilya pas
При успешном вводе открывается доступ к работе с файлами хранилища Storage. Папка Storage хранится в корне лабы. Файл с метаданными Metadata.dat 
о файлах в Storage - также в корне. Максимальный размер файлов в хранилище - 10мб, максимальный размер одного файла - 1мб.
Пути к файлам вводятся без ковычек. Например:
file upload e:\1.txt

Кроме команд из задания есть дополнительные:

storageinfo - для получения информации о хранилище

file search <имя файла с расширением,который находится в Storage> - поиск файла в хранилище

newuser <login> <password> - добавить нового пользователя в приложение

Для выхода из приложения - команда exit



