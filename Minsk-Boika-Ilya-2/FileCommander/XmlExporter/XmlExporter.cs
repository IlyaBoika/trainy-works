﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Export;
using XmlExporter.Format;

namespace XmlExporter
{
    public class XmlExporter : IExporter
    {
        public ExportFormatType Type => ExportFormatType.Xml;

        public void Export(IEnumerable<ExportData> data, string path)
        {
            var formatData = data.Select(_ => new FormatExportData
            {
                Filename = _.Filename,
                Size = _.Size,
                Date = _.Date
            })
            .ToList();

            XmlSerializer serializer = new XmlSerializer(typeof(List<FormatExportData>));

            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(fs, formatData);
            }
        }
    }
}