﻿using System;
using System.Xml.Serialization;

namespace XmlExporter.Format
{
    public class FormatExportData
    {
        public string Filename { get; set; }
        public long Size { get; set; }

        [XmlElement(DataType = "date")]
        public DateTime Date { get; set; }
    }
}