﻿using FileCommander.Domain;
using FileCommander.Repositories.Interfaces;
using FileCommander.Services.Interfaces;

namespace FileCommander.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordService _passwordService;

        public AuthenticationService(IUserRepository userRepository, IPasswordService passwordService)
        {
            _userRepository = userRepository;
            _passwordService = passwordService;
        }

        public User Login(string username, string password)
        {
            var retrievedUser = _userRepository.GetUserByUsername(username);

            if ((retrievedUser == null) || (!(_passwordService.IsRightPassword(retrievedUser.Password, password))))
            {
                return null;
            }

            return retrievedUser;
        }

        public bool TryToAdd(User user)
        {
            var savedPasswordHash = _passwordService.GetPasswordHash(user.Password);

            user.Password = savedPasswordHash;

            return _userRepository.TryToAdd(user);
        }
    }
}