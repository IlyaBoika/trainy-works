﻿using System.Collections.Generic;
using System.Linq;

namespace FileCommander.Services.Models
{
    public class StorageInfo
    {
        public List<StorageFileInfo> Files { get; set; }

        public long Size
        {
            get
            {
                if (Files == null)
                {
                    return 0;
                }

                return Files.Sum(_ => _.Length);
            }
        }
    }
}