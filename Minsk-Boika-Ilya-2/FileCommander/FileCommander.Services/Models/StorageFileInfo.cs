﻿using System;
using System.IO;

namespace FileCommander.Services.Models
{
    public class StorageFileInfo
    {
        public string Filename { get; set; }
        public long Length { get; set; }

        public string Extension
        {
            get
            {
                return Path.GetExtension(Filename);
            }
        }

        public DateTime CreationTime { get; set; }
    }
}