﻿using System.Collections.Generic;
using FileCommander.Domain;

namespace FileCommander.Services.Interfaces
{
    public interface IMetadataService
    {
        void Add(Metadata metadata);
        void Remove(string fileName);
        Dictionary<string, Metadata> GetAllMetadata(string username);
        Metadata SearchByName(string fileName, string username);
        void Rename(string oldName, string newName);
    }
}