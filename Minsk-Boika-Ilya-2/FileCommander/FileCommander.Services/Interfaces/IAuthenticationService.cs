﻿using FileCommander.Domain;

namespace FileCommander.Services.Interfaces
{
    public interface IAuthenticationService
    {
        User Login(string username, string password);

        bool TryToAdd(User user);
    }
}