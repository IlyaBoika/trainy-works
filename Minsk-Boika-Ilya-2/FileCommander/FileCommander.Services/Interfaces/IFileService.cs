﻿using FileCommander.Domain;
using FileCommander.Services.Models;

namespace FileCommander.Services.Interfaces
{
    public interface IFileService
    {
        /// <summary>
        /// Uploads file.
        /// </summary>
        /// <param name="path">Path to upload file from</param>
        /// <returns>Returns file identifier in storage.</returns>
        string Upload(string path, string username);

        void Download(string fileName, string destinationPath, string username);

        void Move(string oldName, string newName, string username);

        void Remove(string fileName, string username);

        StorageFileInfo Info(string fileName, string username);

        StorageInfo StorageInfo(string username);

        StorageFileInfo Search(string fileName, string username);
    }
}