﻿namespace FileCommander.Services.Interfaces
{
    public interface IFileComparing
    {
        bool IsEqual(string filePath1, string filePath2);
    }
}