﻿namespace FileCommander.Services.Interfaces
{
    public interface IMetadataExportService
    {
        void ToJson(string path, string username);

        void ToXml(string path, string username);

        void ToYaml(string path, string username);
    }
}