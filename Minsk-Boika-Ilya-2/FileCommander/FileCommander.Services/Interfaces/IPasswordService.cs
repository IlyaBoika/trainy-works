﻿namespace FileCommander.Services.Interfaces
{
    public interface IPasswordService
    {
        string GetPasswordHash(string password);

        bool IsRightPassword(string savedPasswordHash, string password);
    }
}
