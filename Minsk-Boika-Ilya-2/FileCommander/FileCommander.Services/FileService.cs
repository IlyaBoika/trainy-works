﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using FileCommander.Domain;
using FileCommander.Services.Interfaces;
using FileCommander.Services.Models;
using FileCommander.Toolbox.Logger;

namespace FileCommander.Services
{
    public class FileService : IFileService
    {
        private const long MAX_STORAGE_SIZE = 10485760;
        private const long MAX_FILE_SIZE = 1048576;
        private readonly string _storagePath = ConfigurationManager.AppSettings.Get("storagePath");

        private readonly IFileComparing _hashComparing;
        private readonly IMetadataService _metadataService;

        public FileService(IFileComparing hashComparing, IMetadataService metadataService)
        {
            _hashComparing = hashComparing;
            _metadataService = metadataService;
        }

        public string Upload(string path, string username)
        {
            var fileName = Path.GetFileName(path);
            var nameInStorage = $"{username}_{fileName}";

            var targetPath = Path.Combine(_storagePath, nameInStorage);

            if (_metadataService.GetAllMetadata(username).ContainsKey(fileName))
            {
                throw new ArgumentException("File with the same name is already exists.");
            }

            var fileInf = new FileInfo(path);

            if (!fileInf.Exists)
            {
                throw new ArgumentException("There is no file.");
            }

            var storageSize = _metadataService.GetAllMetadata(username).Values.Sum(s => s.Size);

            if ((fileInf.Length >= MAX_FILE_SIZE) || (storageSize >= MAX_STORAGE_SIZE))
            {
                throw new ArgumentException("File is so big for this storage.");
            }

            try
            {
                fileInf.CopyTo(targetPath);
            }
            catch
            {
                throw new ArgumentException("File hasn't been uploaded");
            }

            var fileIdentifier = fileName;

            _metadataService.Add(new Metadata
            {
                Filename = fileName,
                Size = fileInf.Length,
                CreationTime = fileInf.CreationTime,
                UserUsername = username,
            });

            if (_hashComparing.IsEqual(path, targetPath))
            {
                Logger.Log($"File {fileIdentifier} has been downloaded, process id: {Process.GetCurrentProcess().Id}", LogType.Info);

                return fileIdentifier;
            }

            throw new ArgumentException("Downloaded file is beaten.");
        }

        public void Download(string fileName, string destinationPath, string username)
        {
            var nameInStorage = $"{username}_{fileName}";

            var path = Path.Combine(_storagePath, nameInStorage);
            var targetPath = Path.Combine(destinationPath, fileName);

            var fileInf = new FileInfo(path);

            if (!fileInf.Exists)
            {
                throw new ArgumentException("There is no file.");
            }

            try
            {
                fileInf.MoveTo(targetPath);

                Logger.Log($"File {fileName} has been downloaded, process id: {Process.GetCurrentProcess().Id}", LogType.Info);
            }
            catch
            {
                throw new ArgumentException("File hasn't been uploaded.");
            }
        }

        public void Move(string oldName, string newName, string username)
        {
            var oldNameInStorage = $"{username}_{oldName}";
            var newNameInStorage = $"{username}_{newName}";

            var oldNameFullPath = Path.Combine(_storagePath, oldNameInStorage);
            var newNameFullPath = Path.Combine(_storagePath, newNameInStorage);

            if (_metadataService.GetAllMetadata(username).ContainsKey(newName))
            {
                throw new ArgumentException("File with the same name is already exists.");
            }

            try
            {
                File.Move(oldNameFullPath, newNameFullPath);
            }
            catch
            {
                throw new ArgumentException("File hasn't been renamed.");
            }

            _metadataService.Rename(oldName, newName);

            Logger.Log($"File {oldName} has been moved to {newName}, process id: {Process.GetCurrentProcess().Id}", LogType.Info);
        }

        public void Remove(string fileName, string username)
        {
            var nameInStorage = $"{username}_{fileName}";

            var path = Path.Combine(_storagePath, nameInStorage);

            var fileInf = new FileInfo(path);

            if (!fileInf.Exists)
            {
                throw new ArgumentException("There is no file");
            }

            try
            {
                fileInf.Delete();
            }
            catch
            {
                throw new ArgumentException("File hasn't been removed.");
            }

            _metadataService.Remove(fileName);

            Logger.Log($"File {fileName} has been removed, process id: {Process.GetCurrentProcess().Id}", LogType.Info);
        }

        public StorageFileInfo Info(string fileName, string username)
        {
            var nameInStorage = $"{username}_{fileName}";

            var targetPath = Path.Combine(_storagePath, nameInStorage);

            var fileInf = new FileInfo(targetPath);

            if (!fileInf.Exists)
            {
                throw new ArgumentException("There is no file");
            }

            return new StorageFileInfo
            {
                Filename = fileName,
                Length = fileInf.Length,
                CreationTime = fileInf.CreationTime
            };
        }

        public StorageInfo StorageInfo(string username)
        {
            var metadatas = _metadataService.GetAllMetadata(username);

            var files = metadatas.Select(_ => new StorageFileInfo
            {
                Filename = _.Value.Filename,
                Length = _.Value.Size,
                CreationTime = _.Value.CreationTime
            })
            .ToList();

            return new StorageInfo
            {
                Files = files
            };
        }

        public StorageFileInfo Search(string fileName, string username)
        {
            var searchMetadata = _metadataService.SearchByName(fileName, username);

            return new StorageFileInfo
            {
                Filename = fileName,
                Length = searchMetadata.Size,
                CreationTime = searchMetadata.CreationTime,
            };
        }
    }
}