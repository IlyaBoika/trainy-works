﻿using System;
using System.IO;
using System.Security.Cryptography;
using FileCommander.Services.Interfaces;

namespace FileCommander.Services
{
    public class HashComparing : IFileComparing
    {
        public bool IsEqual(string filePath1, string filePath2)
        {
            string hash;
            string hash2;

            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filePath1))
                {
                    hash = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
                }

                using (var stream = File.OpenRead(filePath2))
                {
                    hash2 = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
                }
            }

            if (hash == hash2)
            {
                return true;
            }

            return false;
        }
    }
}