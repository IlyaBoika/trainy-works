﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Export;
using FileCommander.Domain;
using FileCommander.Repositories;
using FileCommander.Services.Interfaces;
using Newtonsoft.Json;

namespace FileCommander.Services
{
    public class MetadataStandardExportService : IMetadataExportService
    {
        private readonly List<IExporter> _exporters;
        private readonly IMetadataService _metadataService;

        public MetadataStandardExportService(List<IExporter> exporters, IMetadataService metadataService)
        {
            _exporters = exporters;
            _metadataService = metadataService;
        }

        public void ToJson(string path, string username)
        {
            using (var file = File.CreateText(path))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, _metadataService.GetAllMetadata(username).Values.Select(ToExportData).ToList());
            }
        }

        public void ToXml(string path, string username)
        {
            var exporter = _exporters.FirstOrDefault(_ => _.Type == ExportFormatType.Xml);

            exporter?.Export(_metadataService.GetAllMetadata(username).Values.Select(ToExportData).ToList(), path);
        }

        public void ToYaml(string path, string username)
        {
            var exporter = _exporters.FirstOrDefault(_ => _.Type == ExportFormatType.Yaml);

            exporter?.Export(_metadataService.GetAllMetadata(username).Values.Select(ToExportData).ToList(), path);
        }

        private ExportData ToExportData(Metadata metadata)
        {
            return new ExportData
            {
                Filename = metadata.Filename,
                Size = metadata.Size,
                Date = metadata.CreationTime
            };
        }
    }
}