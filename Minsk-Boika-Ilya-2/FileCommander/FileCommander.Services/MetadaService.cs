﻿using System.Collections.Generic;
using FileCommander.Domain;
using FileCommander.Repositories.Interfaces;
using FileCommander.Services.Interfaces;

namespace FileCommander.Services
{
    public class MetadaService : IMetadataService
    {
        private readonly IFileRepository _fileRepository;

        public MetadaService(IFileRepository fileRepository)
        {
            _fileRepository = fileRepository;
        }

        public void Add(Metadata metadata)
        {
            _fileRepository.Add(metadata);
        }

        public Dictionary<string, Metadata> GetAllMetadata(string username)
        {
            return _fileRepository.GetAllMetadata(username);
        }

        public void Remove(string fileName)
        {
            _fileRepository.Remove(fileName);
        }

        public void Rename(string oldName, string newName)
        {
            _fileRepository.Rename(oldName, newName);
        }

        public Metadata SearchByName(string fileName, string username)
        {
            return _fileRepository.SearchByName(fileName, username);
        }
    }
}