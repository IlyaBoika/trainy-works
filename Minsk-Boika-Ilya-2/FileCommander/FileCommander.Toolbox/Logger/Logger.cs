﻿[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Config/log4net.config")]

namespace FileCommander.Toolbox.Logger
{
    public static class Logger
    {
        public static void Log(string message, LogType type = LogType.Debug)
        {
            var _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            switch (type)
            {
                case LogType.Fatal:
                    _log.Fatal(message);
                    break;
                case LogType.Error:
                    _log.Error(message);
                    break;
                case LogType.Warn:
                    _log.Warn(message);
                    break;
                case LogType.Info:
                    _log.Info(message);
                    break;
                default:
                    _log.Debug(message);
                    break;
            }
        }
    }
}