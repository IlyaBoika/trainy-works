﻿namespace FileCommander.Toolbox.Logger
{
    public enum LogType
    {
        Debug,
        Info,
        Warn,
        Error,
        Fatal
    }
}
