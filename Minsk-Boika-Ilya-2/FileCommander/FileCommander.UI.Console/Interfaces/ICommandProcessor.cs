﻿namespace FileCommander.UI.Console.Interfaces
{
    public interface ICommandProcessor
    {
        void Run();
    }
}