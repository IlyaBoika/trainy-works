﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileCommander.Domain;
using FileCommander.Services.Interfaces;
using FileCommander.UI.Console.Interfaces;

namespace FileCommander.UI.Console
{
    public class CommandProcessor : ICommandProcessor
    {
        private delegate void FileActionHandler(List<string> parameters);

        private bool _consoleCycle = true;

        private readonly IAuthenticationService _authenticationService;
        private readonly IFileService _fileService;
        private readonly IMetadataExportService _metadataExportService;

        private User _loggedInUser = null;

        private readonly List<Command> _supportedCommands = new List<Command>
        {
            new Command
            {
                Name = "login",
                IsAuthenticateRequired = false,
            },
            new Command
            {
                Name = "file",
                IsAuthenticateRequired = true,
            },
            new Command
            {
                Name = "user",
                IsAuthenticateRequired = true,
            },
            new Command
            {
                Name = "storageinfo",
                IsAuthenticateRequired = true,
            },
            new Command
            {
                Name = "exit",
                IsAuthenticateRequired = false,
            },
            new Command
            {
                Name = "newuser",
                IsAuthenticateRequired = false,
            }
        };

        private readonly Dictionary<string, FileActionHandler> _fileCommandHandlers;

        public CommandProcessor(IAuthenticationService authenticationService, IFileService fileService, IMetadataExportService exportData)
        {
            _authenticationService = authenticationService;
            _fileService = fileService;
            _metadataExportService = exportData;

            _fileCommandHandlers = new Dictionary<string, FileActionHandler>
            {
                {"upload", UploadFileActionHandler },
                {"download", DownloadFileActionHandler },
                {"move", MoveFileActionHandler },
                {"remove", RemoveFileActionHandler },
                {"info", InfoFileActionHandler },
                {"search", SearchFileActionHandler },
                {"export", ExportFileActionHandler },
            };
        }

        public void Run()
        {
            while (_consoleCycle)
            {
                System.Console.Write("$ ");
                var input = System.Console.ReadLine();

                var command = ParseCommand(input);

                if (command == null)
                {
                    System.Console.WriteLine("Unsupported command.");
                    continue;
                }

                if (command.IsAuthenticateRequired && !IsAuthenticate())
                {
                    System.Console.WriteLine("Authenticate please.");
                    continue;
                }

                Process(command);
            }
        }

        private bool IsAuthenticate()
        {
            return _loggedInUser != null;
        }

        private Command ParseCommand(string input)
        {
            var tokens = input.Trim().Split(' ');
            var commandName = tokens[0];

            var command = _supportedCommands.SingleOrDefault(_ => _.Name == commandName);

            if (command == null)
            {
                return null;
            }

            var parameters = tokens.ToList().GetRange(1, tokens.Length - 1);

            command.Paramaters = parameters;
            return command;
        }

        public void Process(Command command)
        {
            switch (command.Name)
            {
                case "user":

                    if ((command.Paramaters[0] == "info") && (command.Paramaters.Count == 1))
                    {
                        var storageSize = _fileService.StorageInfo(_loggedInUser.Username).Size;
                        System.Console.WriteLine($"login: {_loggedInUser.Username} \ncreation date: ${_loggedInUser.CreationDate} \nstorage used: {storageSize} bytes");
                    }

                    else
                    {
                        System.Console.WriteLine("Unsupported command");
                    }

                    break;

                case "newuser":

                    if (command.Paramaters.Count == 2)
                    {
                        var newUser = new User
                        {
                            Username = command.Paramaters[0],
                            Password = command.Paramaters[1],
                            CreationDate = DateTime.UtcNow.Date
                        };

                        var isAdded = _authenticationService.TryToAdd(newUser);

                        System.Console.WriteLine(isAdded
                        ? $"Hello {newUser.Username}, you are successfully saved"
                        : "User with this login is already exist");
                    }

                    else
                    {
                        System.Console.WriteLine("Please, write your login and password after command <newuser>");
                    }

                    break;

                case "login":

                    if (command.Paramaters.Count <= 1)
                    {
                        System.Console.WriteLine("Write your login and password after command <Login>");

                        break;
                    }

                    var username = command.Paramaters[0];
                    var password = command.Paramaters[1];

                    _loggedInUser = _authenticationService.Login(username, password);

                    System.Console.WriteLine(_loggedInUser != null
                        ? $"Hello {_loggedInUser.Username}"
                        : "Invalid login or password.");

                    break;

                case "file":

                    if (command.Paramaters.Count == 0)
                    {
                        System.Console.WriteLine("Write your command after <file>");

                        break;
                    }

                    var action = command.Paramaters.First();

                    if (_fileCommandHandlers.TryGetValue(action, out var actionHandler))
                    {
                        actionHandler(command.Paramaters);
                    }
                    else
                    {
                        System.Console.WriteLine("Unsupported command");
                    }

                    break;

                case "storageinfo":
                    {
                        if (command.Paramaters.Count >= 1)
                        {
                            System.Console.WriteLine("Unsupported command");
                            break;
                        }

                        var storageInfo = _fileService.StorageInfo(_loggedInUser.Username);

                        StringBuilder sb = new StringBuilder();
                        foreach (var item in storageInfo.Files)
                        {
                            sb = sb.Append("File: " + item.Filename + " creationdate: " + item.CreationTime + " size: " + item.Length + " bytes." + "\n");
                        }

                        System.Console.WriteLine(sb.ToString());
                    }

                    break;

                case "exit":
                    {
                        if (command.Paramaters.Count >= 1)
                        {
                            System.Console.WriteLine("Unsupported command");
                            break;
                        }

                        _consoleCycle = false;
                    }

                    break;
            }
        }

        private void UploadFileActionHandler(List<string> parameters)
        {
            try
            {
                var identifier = _fileService.Upload(parameters[1], _loggedInUser.Username);
                var fileInfo = _fileService.Info(identifier, _loggedInUser.Username);

                System.Console.WriteLine($"File {fileInfo.Filename} has been downloaded \nfile name: {fileInfo.Filename} \nfile size {fileInfo.Length} bytes\nextension {fileInfo.Extension}.");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private void DownloadFileActionHandler(List<string> parameters)
        {
            var fileName = parameters[1];

            try
            {
                _fileService.Download(fileName, parameters[2], _loggedInUser.Username);
                System.Console.WriteLine($"File {fileName} has been downloaded.");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private void MoveFileActionHandler(List<string> parameters)
        {
            var oldName = parameters[1];
            var newName = parameters[2];

            try
            {
                _fileService.Move(oldName, newName, _loggedInUser.Username);
                System.Console.WriteLine($"File {oldName} has been moved to {newName}.");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private void RemoveFileActionHandler(List<string> parameters)
        {
            var fileName = parameters[1];
            try
            {
                _fileService.Remove(fileName, _loggedInUser.Username);
                System.Console.WriteLine($"File {fileName} has been removed.");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private void InfoFileActionHandler(List<string> parameters)
        {
            try
            {
                var fileInfo = _fileService.Info(parameters[1], _loggedInUser.Username);

                System.Console.WriteLine($"Name: {fileInfo.Filename}\nfile name: {fileInfo.Filename}\nextension: {fileInfo.Extension} \nfile size {fileInfo.Length} bytes\ncreation date: {fileInfo.CreationTime}.");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }


        private void SearchFileActionHandler(List<string> parameters)
        {
            var fileName = parameters[1];
            try
            {
                var searchMetadata = _fileService.Search(fileName, _loggedInUser.Username);

                System.Console.WriteLine($"File: {searchMetadata.Filename} creationdate {searchMetadata.CreationTime} size {searchMetadata.Length} bytes.");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private void ExportFileActionHandler(List<string> parameters)
        {
            if (parameters[1] == "--info")
            {
                System.Console.WriteLine($"- json\n- xml\n -yaml");
            }
            else if ((parameters.Count == 2) || ((parameters[2] == "--format") && (parameters[3] == "json")))
            {
                var pathToFile = parameters[1];

                try
                {
                    _metadataExportService.ToJson(pathToFile, _loggedInUser.Username);
                    System.Console.WriteLine($"The meta-information has been exported, path = {pathToFile}");
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
            else if ((parameters[2] == "--format") && (parameters[3] == "xml"))
            {
                var pathToFile = parameters[1];

                try
                {
                    _metadataExportService.ToXml(pathToFile, _loggedInUser.Username);
                    System.Console.WriteLine($"The meta-information has been exported, path = {pathToFile}");
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message, "Xml Exporter not found");
                }
            }
            else if ((parameters[2] == "--format") && (parameters[3] == "yaml"))
            {
                var pathToFile = parameters[1];

                try
                {
                    _metadataExportService.ToYaml(pathToFile, _loggedInUser.Username);

                    System.Console.WriteLine($"The meta-information has been exported, path = {pathToFile}");
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message, "Yaml Exporter not found");
                }
            }
            else
            {
                System.Console.WriteLine("Unsupported command");
            }
        }
    }
}