﻿using System.Collections.Generic;

namespace FileCommander.UI.Console
{
    public class Command
    {
        public string Name { get; set; }

        public bool IsAuthenticateRequired { get; set; }

        public List<string> Paramaters { get; set; }
    }
}