﻿using FileCommander.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FileCommander.EntityFramework.Mapping
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder.HasKey(_ => _.Username);

            builder.Property(_ => _.Username).HasColumnName("Username").HasMaxLength(100).IsRequired();
            builder.Property(_ => _.Password).HasColumnName("Password").HasMaxLength(100).IsRequired();
            builder.Property(_ => _.CreationDate).HasColumnName("CreationDate").IsRequired();

            builder.HasMany(u => u.Metadatas)
                .WithOne(m => m.User)
                .HasForeignKey(u => u.UserUsername);
        }
    }
}
