﻿using FileCommander.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FileCommander.EntityFramework.Mapping
{
    public class MetadataMap : IEntityTypeConfiguration<Metadata>
    {
        public void Configure(EntityTypeBuilder<Metadata> builder)
        {
            builder.ToTable("Metadata");

            builder.HasKey(_ => new { _.Filename, _.UserUsername });

            builder.Property(_ => _.Filename).HasColumnName("Filename").HasMaxLength(100).IsRequired();
            builder.Property(_ => _.Size).HasColumnName("Size").HasMaxLength(100).IsRequired();
            builder.Property(_ => _.CreationTime).HasColumnName("CreationTime").IsRequired();
            builder.Property(_ => _.UserUsername).HasColumnName("UserUsername").IsRequired();
        }
    }
}
