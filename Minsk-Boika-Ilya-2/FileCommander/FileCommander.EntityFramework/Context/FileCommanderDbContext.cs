﻿using FileCommander.Domain;
using FileCommander.EntityFramework.Mapping;
using Microsoft.EntityFrameworkCore;

namespace FileCommander.EntityFramework.Context
{
    public class FileCommanderDbContext : DbContext
    {
        public FileCommanderDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Metadata> Metadatas { get; set; }

        public DbSet<User> Users { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server= ;Database=FileCommander;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MetadataMap());

            modelBuilder.ApplyConfiguration(new UserMap());
        }
    }
}