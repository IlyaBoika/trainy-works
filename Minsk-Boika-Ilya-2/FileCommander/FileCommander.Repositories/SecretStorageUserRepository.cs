﻿using System.Collections.Generic;
using System.Linq;
using FileCommander.Domain;

namespace FileCommander.Repositories
{
    public class SecretStorageUserRepository 
    {
        private readonly List<User> _users;

        public SecretStorageUserRepository(string username, string password)
        {
            _users = new List<User>
            {
                new User
                {
                    Username = username,
                    Password = password
                }
            };
        }

        public User GetUserByUsernameAndPassword(string username, string password)
        {
            return _users.SingleOrDefault(_ => _.Username == username && _.Password == password);
        }

        public bool TryToAdd(User user)
        {
            throw new System.NotImplementedException();
        }
    }
}