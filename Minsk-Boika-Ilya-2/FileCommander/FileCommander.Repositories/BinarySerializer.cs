﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using FileCommander.Domain;
using FileCommander.Repositories.Interfaces;

namespace FileCommander.Repositories
{
    public class BinarySerializer : ISerializer
    {
        private readonly string _metadataPath = ConfigurationManager.AppSettings.Get("metadataPath");

        public void Serialize(Dictionary<string, Metadata> records)
        {
            var formatter = new BinaryFormatter();
            using (var fs = new FileStream(_metadataPath, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, records);
            }
        }
    }
}