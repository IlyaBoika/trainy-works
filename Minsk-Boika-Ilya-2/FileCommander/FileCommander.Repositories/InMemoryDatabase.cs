﻿using System.Collections.Generic;
using FileCommander.Domain;
using FileCommander.Repositories.Interfaces;

namespace FileCommander.Repositories
{
    public class InMemoryDatabase
    {
        public Dictionary<string, Metadata> Records { get; set; }
        public InMemoryDatabase(IDeserilizer binaryDeserializer) => Records = binaryDeserializer.Deserialize();
    }
}