﻿using System.Collections.Generic;
using System.Linq;
using FileCommander.Domain;
using FileCommander.Repositories.Interfaces;

namespace FileCommander.Repositories
{
    public class BinaryFileMetadataRepository : IFileRepository
    {
        private readonly InMemoryDatabase _inMemoryDatabase;
        private readonly ISerializer _binarySerializer;

        public BinaryFileMetadataRepository(InMemoryDatabase inMemoryDatabase, ISerializer binarySerializer)
        {
            _inMemoryDatabase = inMemoryDatabase;
            _binarySerializer = binarySerializer;
        }

        public void Add(Metadata metadata)
        {
            _inMemoryDatabase.Records.Add(metadata.Filename, metadata);

            _binarySerializer.Serialize(_inMemoryDatabase.Records);
        }

        public Dictionary<string, Metadata> GetAllMetadata(string username)
        {
            var metadatas = _inMemoryDatabase.Records.Where(_ => _.Value.UserUsername == username).ToDictionary(_ => _.Key, _ => _.Value);

            return metadatas;
        }

        public void Remove(string fileName)
        {
            _inMemoryDatabase.Records.Remove(fileName);

            _binarySerializer.Serialize(_inMemoryDatabase.Records);
        }

        public void Rename(string oldName, string newName)
        {
            var metadata = _inMemoryDatabase.Records[oldName];
            _inMemoryDatabase.Records.Remove(oldName);
            metadata.Filename = newName;
            _inMemoryDatabase.Records.Add(newName, metadata);

            _binarySerializer.Serialize(_inMemoryDatabase.Records);
        }


        public Metadata SearchByName(string fileName, string username)
        {
            var searchMetadata = _inMemoryDatabase.Records[fileName];

            return searchMetadata;
        }
    }
}