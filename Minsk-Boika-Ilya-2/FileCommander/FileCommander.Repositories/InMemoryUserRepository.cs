﻿using System.Collections.Generic;
using System.Linq;
using FileCommander.Domain;
using FileCommander.Repositories.Interfaces;

namespace FileCommander.Repositories
{
    public class InMemoryUserRepository
    {
        private readonly List<User> _users = new List<User>
        {
            new User
            {
                Username = "ilya",
                Password = "pas"
            },
            new User
            {
                Username = "test",
                Password = "test"
            },
        };

        public User GetUserByUsernameAndPassword(string username, string password)
        {
            return _users.SingleOrDefault(_ => _.Username == username && _.Password == password);
        }

        public bool TryToAdd(User user)
        {
            throw new System.NotImplementedException();
        }
    }
}