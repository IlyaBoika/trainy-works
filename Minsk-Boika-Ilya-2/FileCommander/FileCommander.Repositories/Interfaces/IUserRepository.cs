﻿using FileCommander.Domain;

namespace FileCommander.Repositories.Interfaces
{
    public interface IUserRepository
    {
        User GetUserByUsername(string username);

        bool TryToAdd(User user);
    }
}