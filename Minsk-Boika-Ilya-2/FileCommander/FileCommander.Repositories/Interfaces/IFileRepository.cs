﻿using System.Collections.Generic;
using FileCommander.Domain;

namespace FileCommander.Repositories.Interfaces
{
    public interface IFileRepository
    {
        void Add(Metadata metadata);
        void Remove(string fileName);
        Dictionary<string, Metadata> GetAllMetadata(string username);
        Metadata SearchByName(string fileName, string username);
        void Rename(string oldName, string newName);
    }
}