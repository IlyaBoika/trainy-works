﻿using System.Collections.Generic;
using FileCommander.Domain;

namespace FileCommander.Repositories.Interfaces
{
    public interface IDeserilizer
    {
         Dictionary<string, Metadata> Deserialize();
    }
}