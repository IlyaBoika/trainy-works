﻿using System.Collections.Generic;
using FileCommander.Domain;

namespace FileCommander.Repositories.Interfaces
{
    public interface ISerializer
    {
        void Serialize(Dictionary<string, Metadata> records);
    }
}