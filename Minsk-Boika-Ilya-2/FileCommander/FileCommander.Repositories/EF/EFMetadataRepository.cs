﻿using FileCommander.Domain;
using FileCommander.EntityFramework.Context;
using FileCommander.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace FileCommander.Repositories.EF
{
    public class EFMetadataRepository : IFileRepository
    {
        private readonly FileCommanderDbContext _context;

        public EFMetadataRepository(FileCommanderDbContext context)
        {
            _context = context;
        }

        public void Add(Metadata metadata)
        {
            _context.Add(metadata);
            _context.SaveChanges();
        }

        public Dictionary<string, Metadata> GetAllMetadata(string username)
        {
            return _context.Metadatas.Where(_ => _.UserUsername == username).ToDictionary(_ => _.Filename, _ => _);
        }

        public void Remove(string fileName)
        {
            _context.Metadatas.Remove(_context.Metadatas.First(_ => _.Filename == fileName));
            _context.SaveChanges();
        }

        public void Rename(string oldName, string newName)
        {
            var oldMetadata = _context.Metadatas.First(_ => _.Filename == oldName);
            var newmetadata = new Metadata
            {
                Filename = newName,
                Size = oldMetadata.Size,
                CreationTime = oldMetadata.CreationTime,
                UserUsername = oldMetadata.UserUsername,
            };

            _context.Metadatas.Remove(oldMetadata);
            _context.Add(newmetadata);
            _context.SaveChanges();
        }

        public Metadata SearchByName(string fileName, string username)
        {
            return _context
                  .Metadatas
                  .Where(_ => _.UserUsername == username)
                  .First(_ => _.Filename == fileName);
        }
    }
}