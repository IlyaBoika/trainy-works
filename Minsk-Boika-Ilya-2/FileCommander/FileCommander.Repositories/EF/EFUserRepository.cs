﻿using FileCommander.Domain;
using FileCommander.EntityFramework.Context;
using FileCommander.Repositories.Interfaces;
using System.Linq;

namespace FileCommander.Repositories.EF
{
    public class EFUserRepository : IUserRepository
    {
        private readonly FileCommanderDbContext _context;

        public EFUserRepository(FileCommanderDbContext context)
        {
            _context = context;
        }

        public User GetUserByUsername(string username)
        {
            return _context.Users.SingleOrDefault(_ => _.Username == username);
        }

        public bool TryToAdd(User user)
        {
            if (_context.Users.SingleOrDefault(_ => _.Username == user.Username) != null) return false;

            _context.Add(user);
            _context.SaveChanges();

            return true;
        }
    }
}