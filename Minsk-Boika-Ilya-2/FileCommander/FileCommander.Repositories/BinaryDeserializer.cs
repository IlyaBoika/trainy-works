﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using FileCommander.Domain;
using FileCommander.Repositories.Interfaces;

namespace FileCommander.Repositories
{
    public class BinaryDeserializer : IDeserilizer
    {
        private readonly  string _metadataPath = ConfigurationManager.AppSettings.Get("metadataPath");

        public Dictionary<string, Metadata> Deserialize()
        {
            Dictionary<string, Metadata> records;

            var formatter = new BinaryFormatter();

            //Deserialization from file
            using (var fs = new FileStream(_metadataPath, FileMode.OpenOrCreate))
            {
                if (fs.Length != 0)
                {
                    records = (Dictionary<string, Metadata>) formatter.Deserialize(fs);
                }
                else
                {
                    records = new Dictionary<string, Metadata>();
                }
            }

            return records;
        }
    }
}