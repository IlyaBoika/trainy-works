﻿using System;
using System.Collections.Generic;

namespace FileCommander.Domain
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime CreationDate { get; set; }

        public List<Metadata> Metadatas { get; set; }
    }
}
