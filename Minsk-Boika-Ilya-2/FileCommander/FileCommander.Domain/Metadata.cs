﻿using System;

namespace FileCommander.Domain
{
    [Serializable]
    public class Metadata
    {
        public string Filename { get; set; }
        public long Size { get; set; }
        public DateTime CreationTime { get; set; }

        public string UserUsername { get; set; }

        public User User { get; set; }
    }
}