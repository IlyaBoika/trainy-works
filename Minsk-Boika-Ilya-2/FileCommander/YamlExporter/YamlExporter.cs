﻿using Export;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using YamlDotNet.Serialization;
using YamlExporter.Format;

namespace YamlExporter
{
    public class YamlExporter : IExporter
    {
        public ExportFormatType Type => ExportFormatType.Yaml;

        public void Export(IEnumerable<ExportData> data, string path)
        {
            var formatData = data.Select(_ => new FormatExportData
            {
                Filename = _.Filename,
                Size = _.Size,
                CreationTime = _.Date,
            })
                .ToList();

            var serializer = new Serializer();

            using (TextWriter tw = new StreamWriter(path, false))
            {
                serializer.Serialize(tw, formatData);
            }
        }
    }
}