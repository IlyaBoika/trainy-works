﻿using System;

namespace YamlExporter.Format
{
    public class FormatExportData
    {
        public string Filename { get; set; }
        public long Size { get; set; }
        public DateTime CreationTime { get; set; }
    }
}