﻿using Microsoft.EntityFrameworkCore.Design;
using FileCommander.EntityFramework.Context;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace FileCommander.EntityFramework.Migrations
{
    public class DbContextFactory : IDesignTimeDbContextFactory<FileCommanderDbContext>
    {
        public FileCommanderDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<FileCommanderDbContext>();

            builder.UseSqlServer(@"Server= ;Database=FileCommander;Trusted_Connection=True;");

            return new FileCommanderDbContext(builder.Options);
        }
    }
}