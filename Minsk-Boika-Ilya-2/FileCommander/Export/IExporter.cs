﻿using System.Collections.Generic;

namespace Export
{
    public interface IExporter
    {
        void Export(IEnumerable<ExportData> data, string path);
        ExportFormatType Type { get; }
    }

    public enum ExportFormatType
    {
        Xml,
        Yaml
    }
}