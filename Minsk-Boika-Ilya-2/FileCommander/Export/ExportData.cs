﻿using Newtonsoft.Json;
using System;

namespace Export
{
    public class ExportData
    {
        public string Filename { get; set; }
        public long Size { get; set; }

        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime Date { get; set; }
    }
}