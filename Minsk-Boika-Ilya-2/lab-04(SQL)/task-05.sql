--5.1
USE AdventureWorks2017;
GO
CREATE TABLE Person_New (
   BusinessEntityID INT NOT NULL,
   PersonType nchar(2) NOT NULL,
   NameStyle NameStyle NOT NULL,
   Title nvarchar(8) NULL,
   FirstName Name NOT NULL,
   MiddleName Name NULL,
   LastName Name NOT NULL,
   Suffix nvarchar(10) NULL,
   EmailPromotion int NOT NULL,
   ModifiedDate datetime NOT NULL,
);

--5.2
ALTER TABLE Person_New
ADD Salutation nvarchar(80);

--5.3
INSERT INTO Person_New (BusinessEntityID, PersonType, NameStyle, FirstName, MiddleName, LastName, Suffix, EmailPromotion, ModifiedDate, Title)
(
SELECT P.BusinessEntityID, PersonType, NameStyle, FirstName, MiddleName, LastName, Suffix, EmailPromotion, P.ModifiedDate,
CASE
WHEN E.Gender LIKE 'M' THEN 'Mr.'
WHEN E.Gender LIKE 'F' THEN 'Ms.'
END Title
FROM Person AS P
INNER JOIN HumanResources.Employee AS E
ON E.BusinessEntityID  = P.BusinessEntityID
)

--5.4
UPDATE Person_New
SET Salutation = Title +' '+ FirstName

--5.5
DELETE FROM Person_New
WHERE LEN(Salutation) > 10

--5.6
ALTER TABLE Person
DROP CONSTRAINT CHK_PERSON_MiddleName;

ALTER TABLE Person
DROP CONSTRAINT DF__Person__Title__4EA8A765;

ALTER TABLE Person
DROP CONSTRAINT PK__Person__AA2FFBE562346B8D;

--5.7
ALTER TABLE Person
DROP COLUMN PersonId;

--5.8
DROP TABLE Person
DROP TABLE Person_New