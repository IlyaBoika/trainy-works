﻿CREATE TABLE dbo.MyEmployees
(
    EmployeeID int NOT NULL,
    FirstName nvarchar(30)  NOT NULL,
    LastName  nvarchar(40) NOT NULL,
    Title nvarchar(50) NOT NULL,
    DeptID int NOT NULL,
    ManagerID int NULL,
 CONSTRAINT PK_EmployeeID PRIMARY KEY CLUSTERED (EmployeeID ASC) 
);

INSERT INTO dbo.MyEmployees VALUES 
 (1, N'Ken', N'Sánchez', N'Chief Executive Officer',16,0)
,(273, N'Brian', N'Welcker', N'Vice President of Sales',3,1)
,(274, N'Stephen', N'Jiang', N'North American Sales Manager',3,273)
,(275, N'Michael', N'Blythe', N'Sales Representative',3,274)
,(276, N'Linda', N'Mitchell', N'Sales Representative',3,274)
,(285, N'Syed', N'Abbas', N'Pacific Sales Manager',3,273)
,(286, N'Lynn', N'Tsoflias', N'Sales Representative',3,285)
,(16,  N'David',N'Bradley', N'Marketing Manager', 4, 273)
,(23,  N'Mary', N'Gibson', N'Marketing Specialist', 4, 16);


GO
--RecursiveQuery
--chain from employee with ID=23 to main boss
WITH RecursiveQuery (EmployeeID, ManagerID, FirstName, LastName) 
AS
(
SELECT EmployeeID, ManagerID, FirstName, LastName
FROM MyEmployees  AS E 
WHERE E.EmployeeID = 23
UNION ALL
SELECT E.EmployeeID, E.ManagerID, E.FirstName, E.LastName
FROM MyEmployees AS E 
JOIN RecursiveQuery AS R 
ON E.EmployeeID = R.ManagerID
) 
SELECT *
FROM RecursiveQuery 

