-- Restore database AdventureWorks2017
USE master
GO
    RESTORE DATABASE AdventureWorks2017
    FROM DISK = 'e:\lab-04(SQL)\AdventureWorks2017.bak'
	WITH 
	MOVE 'AdventureWorks2017' TO 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\AdventureWorks2017.mdf',
	MOVE 'AdventureWorks2017_log' TO 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\AdventureWorks2017_log.ldf',
	REPLACE,
	STATS=10

--1 Request
SELECT DISTINCT TOP (8) [DepartmentID]
      ,[Name]
      ,[GroupName]
      ,[ModifiedDate]
  FROM [AdventureWorks2017].[HumanResources].[Department]
ORDER BY Name DESC;

--2 Request
SELECT [BusinessEntityID]
      ,[NationalIDNumber]
      ,[JobTitle]
      ,[BirthDate]
      ,[HireDate]
  FROM [AdventureWorks2017].[HumanResources].[Employee]
  WHERE DATEDIFF(YEAR,BirthDate,HireDate)=22;

--3 Request
SELECT [BusinessEntityID]
      ,[NationalIDNumber]
      ,[JobTitle]
      ,[BirthDate]
      ,[MaritalStatus]
  FROM [AdventureWorks2017].[HumanResources].[Employee]
  WHERE MaritalStatus='M' AND JobTitle IN ('Design Engineer','Tool Designer','Engineering Manager','Production Control Manager')
  ORDER BY BirthDate;

--4 Request
  SELECT  [BusinessEntityID] AS B
      ,[JobTitle]
      ,[BirthDate]
      ,[Gender]
      ,[HireDate]
  FROM [AdventureWorks2017].[HumanResources].[Employee]
  Where Month(HireDate) = 3 AND Day(HireDate) = 5
  ORDER BY B
  OFFSET 1 ROW
  FETCH NEXT 5 ROWS ONLY

--5 Request
SELECT [BusinessEntityID]
      ,[JobTitle]
	  ,[Gender]
	  ,[HireDate]
      ,REPLACE([LoginID], 'adventure-works', 'adventure-works2017') AS LoginID
  FROM [AdventureWorks2017].[HumanResources].[Employee]
  WHERE  DATENAME(dw,HireDate)= 'Wednesday' AND Gender = 'F';

--6 Request
  SELECT SUM(VacationHours) as VacationSumInHours, SUM(SickLeaveHours) as SicknessSumInHours     
  FROM [AdventureWorks2017].[HumanResources].[Employee]

--7 Request
  SELECT  DISTINCT TOP(8) 
      [JobTitle]
	  ,RIGHT(RTRIM(JobTitle), CHARINDEX(' ', REVERSE(RTRIM(JobTitle)) + ' ') - 1) as LastWord
  FROM [AdventureWorks2017].[HumanResources].[Employee]
  ORDER BY JobTitle DESC

--8 Request
SELECT [BusinessEntityID]
      ,[JobTitle]
	  ,[Gender]
	  ,[BirthDate]
	  ,[HireDate]
  FROM [AdventureWorks2017].[HumanResources].[Employee]
  WHERE  JobTitle like '%Control%'


