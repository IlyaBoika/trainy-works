--4.1.1
USE AdventureWorks2017;
GO

CREATE TABLE Person(
   BusinessEntityID INT NOT NULL,
   PersonType nchar(2) NOT NULL,
   NameStyle NameStyle NOT NULL,
   Title nvarchar(8) NULL,
   FirstName Name NOT NULL,
   MiddleName Name NULL,
   LastName Name NOT NULL,
   Suffix nvarchar(10) NULL,
   EmailPromotion int NOT NULL,
   ModifiedDate datetime NOT NULL,
);

--4.1.2
ALTER TABLE Person
ADD PersonId INT PRIMARY KEY IDENTITY(3,5);

--4.1.3
ALTER TABLE Person
ADD CONSTRAINT CHK_PERSON_MiddleName CHECK (MiddleName = 'J' or MiddleName = 'L');

--4.1.4
ALTER TABLE Person
ADD DEFAULT ('N/A' ) FOR Title;

--4.1.5
INSERT INTO Person (BusinessEntityID, PersonType, NameStyle, Title, FirstName, MiddleName, LastName, Suffix, EmailPromotion, ModifiedDate)
(
SELECT P.BusinessEntityID, PersonType, NameStyle, Title, FirstName, MiddleName, LastName, Suffix, EmailPromotion, P.ModifiedDate
FROM Person.Person AS P
INNER JOIN HumanResources.Employee AS E
ON P.BusinessEntityID = E.BusinessEntityID
INNER JOIN HumanResources.EmployeeDepartmentHistory AS EDH
ON P.BusinessEntityID = EDH.BusinessEntityID
INNER JOIN HumanResources.Department AS D
ON EDH.DepartmentID = D.DepartmentID
WHERE D.Name != 'Finance' AND (MiddleName = 'J' OR MiddleName = 'L')
)