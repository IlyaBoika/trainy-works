--Create database
CREATE DATABASE Something

--Check database
SELECT name, create_date
FROM sys.databases;

--Create table Wicked
USE Something; 
CREATE TABLE Wicked 
(
    Id INT NOT NULL,
);

--Backup database Something
BACKUP DATABASE Something
TO DISK = 'e:\lab-04(SQL)\Something.bak'

--Drop database Something
USE master;  
GO  
DROP DATABASE Something ;

--Restore database Something
USE master
GO
    RESTORE DATABASE Something
    FROM DISK = 'e:\lab-04(SQL)\Something.bak'