--4.2.1
USE AdventureWorks2017;
GO

CREATE TABLE StateProvince(
	StateProvinceID int PRIMARY KEY NOT NULL,
	StateProvinceCode nchar(3) NOT NULL,
	CountryRegionCode nvarchar(3) NOT NULL,
	IsOnlyStateProvinceFlag Flag NOT NULL,
	Name Name NOT NULL,
	TerritoryID int NOT NULL,
	ModifiedDate datetime NOT NULL,
);

--4.2.2
ALTER TABLE StateProvince
ADD UNIQUE (Name) ;

--4.2.3
ALTER TABLE StateProvince
ADD CHECK (CountryRegionCode LIKE '%[^0-9]%'); 

--4.2.4
ALTER TABLE StateProvince
ADD DEFAULT GETDATE() FOR  ModifiedDate ;

--4.2.5
INSERT INTO StateProvince(StateProvinceID, StateProvinceCode, CountryRegionCode, IsOnlyStateProvinceFlag, Name, TerritoryID, ModifiedDate)
(
SELECT StateProvinceID, StateProvinceCode, SP.CountryRegionCode, IsOnlyStateProvinceFlag, SP.Name, TerritoryID, SP.ModifiedDate
FROM Person.StateProvince AS SP
INNER JOIN Person.CountryRegion AS CR
ON SP.CountryRegionCode = CR.CountryRegionCode
WHERE SP.Name = CR.Name
)

--4.2.6
ALTER TABLE StateProvince
DROP COLUMN IsOnlyStateProvinceFlag ;
GO
ALTER TABLE StateProvince
ADD Population INT NULL;