USE AdventureWorks2017;
GO

--Request 1
SELECT E.BusinessEntityID, E.JobTitle, MAX(EPH.Rate) AS MaxRate 
FROM [AdventureWorks2017].[HumanResources].[EmployeePayHistory] AS EPH
INNER JOIN
[AdventureWorks2017].[HumanResources].[Employee] AS E
ON EPH.BusinessEntityID = E.BusinessEntityID
GROUP BY  E.JobTitle, E.BusinessEntityID;
GO

--Request 2
SELECT E.BusinessEntityID, E.JobTitle, EPH.Rate, DENSE_RANK() OVER (ORDER BY EPH.Rate) AS RateRank
FROM HumanResources.Employee AS E
INNER JOIN HumanResources.EmployeePayHistory AS EPH
ON E.BusinessEntityID = EPH.BusinessEntityID
GO

--Request 3
SELECT E.BusinessEntityID, E.JobTitle, COUNT(EPH.Rate) RateCount 
FROM HumanResources.Employee AS E 
INNER JOIN HumanResources.EmployeePayHistory AS EPH
ON E.BusinessEntityID = EPH.BusinessEntityID
GROUP BY E.BusinessEntityID, E.JobTitle
HAVING COUNT(EPH.Rate) > 1
ORDER BY COUNT(EPH.Rate) DESC
GO

--Request 4
SELECT D.DepartmentID, D.Name, COUNT(EDH.DepartmentID) AS EmployeeCount
FROM HumanResources.Department AS D 
INNER JOIN HumanResources.EmployeeDepartmentHistory AS EDH
ON D.DepartmentID = EDH.DepartmentID
WHERE EDH.EndDate IS NULL
GROUP BY D.DepartmentID, D.Name
GO

--Request 5
SELECT E.BusinessEntityID, E.JobTitle, EPH.Rate,
LAG (EPH.Rate, 1, 0) OVER (PARTITION BY E.BusinessEntityID ORDER BY E.BusinessEntityID) AS PrevRate,
EPH.Rate - LAG (EPH.Rate, 1, 0) OVER (PARTITION BY E.BusinessEntityID ORDER BY E.BusinessEntityID) AS DiffRate
FROM HumanResources.Employee AS E
INNER JOIN HumanResources.EmployeePayHistory AS EPH
ON E.BusinessEntityID = EPH.BusinessEntityID
GO

--Request 6
SELECT D.Name, EDH.BusinessEntityID, EPH.Rate,
MAX(EPH.Rate) OVER (PARTITION BY D.Name) AS MaxInDepartment,
DENSE_RANK() OVER (PARTITION BY D.Name ORDER BY EPH.Rate) AS RateGroup
FROM HumanResources.EmployeePayHistory AS EPH
INNER JOIN HumanResources.EmployeeDepartmentHistory AS EDH
ON EDH.BusinessEntityID = EPH.BusinessEntityID
INNER JOIN HumanResources.Department AS D
ON D.DepartmentID = EDH.DepartmentID
WHERE EDH.EndDate IS NULL;
GO

--Request 7
SELECT EDH.BusinessEntityID, E.JobTitle, S.Name, S.StartTime, S.EndTime
FROM HumanResources.EmployeeDepartmentHistory AS EDH
INNER JOIN HumanResources.Shift AS S
On EDH.ShiftID = S.ShiftID
INNER JOIN HumanResources.Employee AS E
ON EDH.BusinessEntityID = E.BusinessEntityID
WHERE S.Name = 'Evening'
GO

--Request 8
SELECT E.BusinessEntityID, E.JobTitle, EDH.StartDate, EDH.EndDate, DATEDIFF(year, StartDate, ISNULL(EDH.EndDate, GETDATE())) AS Experience
FROM HumanResources.Employee AS E
INNER JOIN HumanResources.EmployeeDepartmentHistory AS EDH
ON E.BusinessEntityID = EDH.BusinessEntityID
INNER JOIN HumanResources.Department AS D
On EDH.DepartmentID = D.DepartmentID

